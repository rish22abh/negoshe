package com.iws.negoshe.app.activity

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException


class ForgotPasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val window = window
        val background = ContextCompat.getDrawable(this@ForgotPasswordActivity, R.drawable.gradient)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this@ForgotPasswordActivity, android.R.color.transparent)
        window.setBackgroundDrawable(background)
        setContentView(R.layout.activity_forgot_password)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        inputEmail.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Bold.otf")
        toolbarTitle.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-SemiBold.otf")
        buttonSubmit.setOnClickListener {
            //            if (isValidEmail(inputEmail.text.toString()))
            hideKeyboard()
            onClickForgot(inputEmail.text.toString())
//            else
//                Toast.makeText(this, "Enter valid email id", Toast.LENGTH_SHORT).show()
        }

    }

    fun hideKeyboard() {

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(inputEmail.windowToken, 0)

    }
    private fun onClickForgot(email: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_email", email)
        forgot(jsonObject)
    }

    fun forgot(forgot: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(this@ForgotPasswordActivity).create(ApiClient.ApiInterface::class.java)
        val call = apiService.forgot_password(forgot)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                progressDialog.dismiss()
                Log.e("response_onFailure", "login-${t!!.message}")
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(this@ForgotPasswordActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(this@ForgotPasswordActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(this@ForgotPasswordActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                progressDialog.dismiss()
                Log.e("response_onResponse", "login-${response!!.body().toString()}"+response.code())
//                if (!response.body()!!.toString().equals(null)||!response.body()!!.toString().equals("null"))
                if (response.code() == 200||response.code() == 201||response.code() == 202) {
                    if (response.body()!!.isJsonObject) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            Toast.makeText(this@ForgotPasswordActivity, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            inputEmail.setText("")
                        } else {
                            if (response.body()!!.has("message"))
                                Toast.makeText(this@ForgotPasswordActivity, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(this@ForgotPasswordActivity, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(this@ForgotPasswordActivity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}
