package com.iws.negoshe.app.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.mechanic.activity.MechanicActivity
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException


class LoginActivity : AppCompatActivity() {
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        edtUsername.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtPassword.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        textForgotpass.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Light.otf")
        textViewLogin.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Bold.otf")
        textSignup.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Regular.otf")

        textForgotpass.setOnClickListener {
            startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
        }
        textSignup.setOnClickListener {
            startActivity(Intent(this@LoginActivity, SignupActivity::class.java))
            finish()
        }
        cardviewSubmit.setOnClickListener {
            hideKeyboard()
            onClickLogin(edtUsername.text.toString(), edtPassword.text.toString())

        }

        sharedprferencesDetails = SharedprferencesDetails.getInstance(this@LoginActivity)
    }

    private fun onClickLogin(username: String, password: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)
        jsonObject.addProperty("push_token", sharedprferencesDetails!!.getString(SharedprferencesDetails.registrationId))
        Log.e("request_login", "login-$jsonObject")
        login(jsonObject)

    }

    fun hideKeyboard() {

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(edtUsername.windowToken, 0)

    }
    fun login(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(this@LoginActivity).create(ApiClient.ApiInterface::class.java)
        val call = apiService.user_login(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()

                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(this@LoginActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(this@LoginActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(this@LoginActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200||response.code() == 201||response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            val objectData: JsonObject = response.body()!!.get("data").asJsonObject
                            val sharedPreferencesDetails: SharedprferencesDetails = SharedprferencesDetails.getInstance(this@LoginActivity)
                            sharedPreferencesDetails.putString(SharedprferencesDetails.website, when (objectData.get("website").isJsonNull) {true -> ""
                                false -> objectData.get("website").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.username, when (objectData.get("username").isJsonNull) {true -> ""
                                false -> objectData.get("username").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.last_name, when (objectData.get("last_name").isJsonNull) {true -> ""
                                false -> objectData.get("last_name").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.profile_pic, when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.is_active, when (objectData.get("is_active").isJsonNull) {true -> ""
                                false -> objectData.get("is_active").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.profile_owner, when (objectData.get("profile_owner").isJsonNull) {true -> ""
                                false -> objectData.get("profile_owner").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.address, when (objectData.get("address").isJsonNull) {true -> ""
                                false -> objectData.get("address").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.first_name, when (objectData.get("first_name").isJsonNull) {true -> ""
                                false -> objectData.get("first_name").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.job_nature, when (objectData.get("job_nature").isJsonNull) {true -> ""
                                false -> objectData.get("job_nature").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.mobile, when (objectData.get("mobile").isJsonNull) {true -> ""
                                false -> objectData.get("mobile").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.wallet_amount, when (objectData.get("wallet_amount").isJsonNull) {true -> ""
                                false -> objectData.get("wallet_amount").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.zipcode, when (objectData.get("zipcode").isJsonNull) {true -> ""
                                false -> objectData.get("zipcode").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.email, when (objectData.get("email").isJsonNull) {true -> ""
                                false -> objectData.get("email").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.user_id, when (objectData.get("user_id").isJsonNull) {true -> ""
                                false -> objectData.get("user_id").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.banner_img1, when (objectData.get("banner_img1").isJsonNull) {true -> ""
                                false -> ApiClient.BASE_URL_Image + objectData.get("banner_img1").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.banner_img3, when (objectData.get("banner_img3").isJsonNull) {true -> ""
                                false -> ApiClient.BASE_URL_Image + objectData.get("banner_img3").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.banner_img4, when (objectData.get("banner_img4").isJsonNull) {true -> ""
                                false -> ApiClient.BASE_URL_Image + objectData.get("banner_img4").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.banner_img2, when (objectData.get("banner_img2").isJsonNull) {true -> ""
                                false -> ApiClient.BASE_URL_Image + objectData.get("banner_img2").asString
                            })
                            sharedPreferencesDetails.putString(SharedprferencesDetails.password, edtPassword.text.toString())
                            if (when (objectData.get("profile_owner").isJsonNull) {true -> ""
                                        false -> objectData.get("profile_owner").asString
                                    }.equals(1.toString(), true)) {
                                startActivity(Intent(this@LoginActivity, com.iws.negoshe.user.activity.UserActivity::class.java))
                            } else if (when (objectData.get("profile_owner").isJsonNull) {true -> ""
                                        false -> objectData.get("profile_owner").asString
                                    }.equals("0", true)) {
                                startActivity(Intent(this@LoginActivity, MechanicActivity::class.java))
                            }
                            finish()
                        } else {
                            if (response.body()!!.has("message"))
                                Toast.makeText(this@LoginActivity, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(this@LoginActivity, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(this@LoginActivity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }

        })
    }
}
