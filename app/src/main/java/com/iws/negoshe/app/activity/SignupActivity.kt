package com.iws.negoshe.app.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.activity_signup.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

class SignupActivity : AppCompatActivity() {

    private var radioButtonUserType: String? = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioButtonMechanic -> radioButtonUserType = "0"
                R.id.radioButtonUser -> radioButtonUserType = "1"
            }
        }

        edtFirstName.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtLastName.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtEmail.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtPhone.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtPassword.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        edtConfirmPassword.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        textSignup.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Bold.otf")
        textLogin.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Regular.otf")
        radioButtonUser.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")
        radioButtonMechanic.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-Medium.otf")


        cardviewSubmit!!.setOnClickListener {
            hideKeyboard()
            if (edtFirstName.text.toString().isEmpty())
                Toast.makeText(this@SignupActivity, "Enter First Name.", Toast.LENGTH_SHORT).show()
            else if (edtLastName.text.toString().isEmpty())
                Toast.makeText(this@SignupActivity, "Enter Last Name.", Toast.LENGTH_SHORT).show()
            else if (edtEmail.text.toString().isEmpty())
                Toast.makeText(this@SignupActivity, "Enter valid email.", Toast.LENGTH_SHORT).show()
            else if (edtPhone.text.toString().isEmpty())
                Toast.makeText(this@SignupActivity, "Enter valid Phone number.", Toast.LENGTH_SHORT).show()
            else if (edtPassword.text.toString().length < 8)
                Toast.makeText(this@SignupActivity, "Enter min 8 digit password.", Toast.LENGTH_SHORT).show()
            else if (edtConfirmPassword.text.toString().length < 8)
                Toast.makeText(this@SignupActivity, "Enter min 8 digit confirm password.", Toast.LENGTH_SHORT).show()
            else if (!edtPassword.text.toString().equals(edtConfirmPassword.text.toString(), true))
                Toast.makeText(this@SignupActivity, "Password and Confirm password does not match.", Toast.LENGTH_SHORT).show()
            else {
                onClickSignup(edtFirstName.text.toString(), edtLastName.text.toString(), edtEmail.text.toString(), edtPhone.text.toString(), edtPassword.text.toString(), radioButtonUserType!!)
            }
        }

        textLogin.setOnClickListener {
            startActivity(Intent(this@SignupActivity, LoginActivity::class.java))
            finish()
        }
    }

    fun hideKeyboard() {

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(edtPassword.windowToken, 0)

    }
    private fun onClickSignup(firstName: String, lastName: String, email: String, phone: String, password: String, radioButtonUserType: String) {
        signUp(firstName, lastName, email, phone, password, radioButtonUserType)
    }

    private fun signUp(firstName: String, lastName: String, email: String, phone: String, password: String, radioButtonUserType: String) {

        val progressDialog: ProgressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(this@SignupActivity).create(ApiClient.ApiInterface::class.java)
        val call = apiService.user_register(password, email, firstName, lastName, phone, radioButtonUserType)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "signup-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(this@SignupActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(this@SignupActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(this@SignupActivity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "signup-${t!!.body().toString()}")
                progressDialog.dismiss()
                if (t.code() == 200||t.code() == 201||t.code() == 202) {
                    if (t.body()!!.isJsonObject) {
                        val intStatus: String = t.body()!!.get("status").asString
                        if (intStatus.equals("1")) {
                            Toast.makeText(this@SignupActivity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this@SignupActivity, LoginActivity::class.java))
                            finish()
                        } else {
                            when {
                                t.body()!!.has("message") -> Toast.makeText(this@SignupActivity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("data") -> Toast.makeText(this@SignupActivity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("error") -> Toast.makeText(this@SignupActivity, "" + t.body()!!.get("error").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(this@SignupActivity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
