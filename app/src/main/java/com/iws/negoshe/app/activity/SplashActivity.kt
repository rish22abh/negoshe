package com.iws.negoshe.app.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.mechanic.activity.MechanicActivity


class SplashActivity : AppCompatActivity(), Runnable {
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    override fun run() {
        finish()
        if (sharedprferencesDetails!!.getString(SharedprferencesDetails.email).equals("", true))
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        else {
            Log.e("splash", sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
            if (sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner).replace("\"", "").equals("1", true))
                startActivity(Intent(this@SplashActivity, com.iws.negoshe.user.activity.UserActivity::class.java))
            else if (sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner).equals("0", true))
                startActivity(Intent(this@SplashActivity, MechanicActivity::class.java))
        }
    }

    private var handler: Handler? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        hideStatusBar()
        setContentView(R.layout.activity_splash)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(this@SplashActivity)

//        Picasso.with(this).load(R.drawable.img_splash).into(imageview)
    }

    override fun onResume() {
        super.onResume()
        handler = Handler()
        handler!!.postDelayed(this@SplashActivity, resources.getInteger(R.integer.splash_time).toLong())
    }

    override fun onPause() {
        super.onPause()
        if (handler != null)
            handler!!.removeCallbacks(this)
    }

    fun hideStatusBar() {

        //Hide the status bar on Android 4.0 and Lower
//        if (Build.VERSION.SDK_INT < 16) {
//            val w = window
//            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN)
//
//        } else {
        val decorView = window.decorView
        // Hide the status bar.
        val visibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.systemUiVisibility = visibility
        // Hide action bar that too if necessary.
//            val actionBar = actionBar
//            actionBar!!.hide()

//        }

    }
}
