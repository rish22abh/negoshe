package com.iws.negoshe.app.service

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.iws.commontask.utils.SharedprferencesDetails


class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.e("token", "Refreshed token: " + refreshedToken!!)
        val sharedprferencesDetails: SharedprferencesDetails = SharedprferencesDetails.getInstance(this@MyFirebaseInstanceIDService)
        sharedprferencesDetails.putString(SharedprferencesDetails.registrationId, refreshedToken)
    }
}