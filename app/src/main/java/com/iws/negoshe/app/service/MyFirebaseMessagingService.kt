package com.iws.negoshe.app.service

import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.SplashActivity
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)

        Log.e("MyFirebaseMessaging", "MyFirebaseMessagingService=" + p0!!.notification!!.body.toString())
        notificationCreate(p0.notification!!.body.toString())
    }

    private fun notificationCreate(toString: String) {
        val intent = Intent(this, SplashActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val mBuilder = NotificationCompat.Builder(this, "0")
                .setSmallIcon(R.drawable.ic_mechanic)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(toString)
//                .setStyle(NotificationCompat.BigTextStyle()
//                        .bigText("Much longer text that cannot fit one line sjdkjsdkjsd kjsdksjdskjd ksjdksjdksdk kjsdkjskdj ksjdksjd kjsdkj ksjdksjd ksjd kjsd jksdj dkldj skdjkjc "))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setSound(alarmSound)
                .setAutoCancel(true)
        val notificationManager = NotificationManagerCompat.from(this)
        val m: Int = ((Date().time / 1000L) % Integer.MAX_VALUE).toInt()
        notificationManager.notify(m, mBuilder.build())
    }
}