package com.iws.negoshe.mechanic.activity

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.Toast
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.utils.CustomTypefaceSpan
import com.iws.negoshe.mechanic.fragment.*
import com.iws.negoshe.mechanic.inerfacePkg.FragmentCommunicationMech
import kotlinx.android.synthetic.main.activity_mechanic.*


class MechanicActivity : AppCompatActivity(), FragmentCommunicationMech {
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun changeFragment(fragment: Fragment, title: String) {
        addFragmentBackStack(fragment, title)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_myjobs -> {
                if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.address).equals("")) {
                    clearBackStack()
                    addFragment(DashboardFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
//                    addFragment(TemporaryMechFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
                    hideBackArrow()
                    imgSearch.visibility = View.VISIBLE
                } else {
                    Toast.makeText(this@MechanicActivity, getString(R.string.update_address), Toast.LENGTH_SHORT).show()
//                    addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
                    Handler().postDelayed(Runnable {
                        navigation.menu.getItem(4).isChecked = true
                    }, 100)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_mybid -> {
                if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.address).equals("")) {
                    clearBackStack()
                    addFragment(MyBidFragment.newInstance("", ""), "My Bid")
//                    addFragment(TemporaryMechFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
                    hideBackArrow()
                    imgSearch.visibility = View.GONE
                } else {
                    Toast.makeText(this@MechanicActivity, getString(R.string.update_address), Toast.LENGTH_SHORT).show()
//                    addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
                    Handler().postDelayed(Runnable {
                        navigation.menu.getItem(4).isChecked = true
                    }, 100)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_car -> {
                if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.address).equals("")) {
                    clearBackStack()
                    addFragment(DashboardFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
                    hideBackArrow()
                    imgSearch.visibility = View.GONE
                    Handler().postDelayed(Runnable {
                        navigation.menu.getItem(0).isChecked = true
                    }, 200)
                } else {
                    Toast.makeText(this@MechanicActivity, getString(R.string.update_address), Toast.LENGTH_SHORT).show()
//                    addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
                    Handler().postDelayed(Runnable {
                        navigation.menu.getItem(4).isChecked = true
                    }, 100)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_mypost -> {
                if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.address).equals("")) {
                    clearBackStack()
//                    addFragment(TemporaryMechFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
                    addFragment(PostUserMechFragment.newInstance("", ""), "Posts")
                    hideBackArrow()
                    imgSearch.visibility = View.GONE
                } else {
                    Toast.makeText(this@MechanicActivity, getString(R.string.update_address), Toast.LENGTH_SHORT).show()
//                    addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
                    Handler().postDelayed(Runnable {
                        navigation.menu.getItem(4).isChecked = true
                    }, 100)
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_myprofile -> {
                clearBackStack()
                addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
                hideBackArrow()
                imgSearch.visibility = View.GONE
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun clearBackStack() {
        val fragmentManager = supportFragmentManager
        while (fragmentManager.backStackEntryCount != 0) {
            fragmentManager.popBackStackImmediate()
        }
    }

    //Add Dynamic fragment to framelayout
    private fun addFragment(fragment: Fragment, title: String) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayout, fragment, title)
        fragmentTransaction.commitAllowingStateLoss()

        toolbarTitle!!.text = title
    }

    //Add Dynamic fragment to framelayout with maintaining backstack
    private fun addFragmentBackStack(fragment: Fragment, title: String) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayout, fragment, title)
        fragmentTransaction.addToBackStack(title)
        fragmentTransaction.commitAllowingStateLoss()
        toolbarTitle!!.text = title
        showBackArrow()
    }


    //show back arrow and set title in center
    private fun showBackArrow() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        )
        params.setMargins(0, 0, 72, 0)
        toolbarTitle.layoutParams = params
        imgSearch.visibility = View.GONE
    }

    //Hide back arrow and set title in center
    private fun hideBackArrow() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        )
        params.setMargins(0, 0, 0, 0)
        toolbarTitle.layoutParams = params
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val background = resources.getDrawable(R.drawable.gradient_mechanic)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = resources.getColor(android.R.color.transparent)
//        window.navigationBarColor = resources.getColor(android.R.color.transparent)
        window.setBackgroundDrawable(background)
        setContentView(R.layout.activity_mechanic)
        setSupportActionBar(toolbar)
        toolbarTitle.typeface = Typeface.createFromAsset(assets, "fonts/Montserrat-SemiBold.otf")


        sharedprferencesDetails = SharedprferencesDetails.getInstance(this)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        BottomNavigationViewHelper.disableShiftMode(navigation)
//        navigation.menu.getItem(2).isEnabled = false
        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.address).equals(""))
            addFragment(DashboardFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
        else {
            imgSearch.visibility = View.GONE
            Toast.makeText(this@MechanicActivity, getString(R.string.update_address), Toast.LENGTH_SHORT).show()
            addFragment(ProfileMechFragment.newInstance("", ""), getString(R.string.title_toolbar_profile))
            navigation.menu.getItem(4).isChecked = true
//            navigation.menu.getItem(0).isEnabled = false
//            navigation.menu.getItem(1).isEnabled = false
//            navigation.menu.getItem(2).isEnabled = false
//            navigation.menu.getItem(3).isEnabled = false
        }

        imgSearch.setOnClickListener {
            addFragmentBackStack(SearchFragment.newInstance("", ""), "Search")
            showBackArrow()
        }


        val m: Menu = navigation.menu
        for (i in 0 until m.size()) {
            val mi = m.getItem(i)
            val subMenu = mi.subMenu
            if (subMenu != null && subMenu.size() > 0) {
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    applyFontToMenuItem(subMenuItem)

                }
            }
            applyFontToMenuItem(mi)
        }
    }

    private fun applyFontToMenuItem(mi: MenuItem) {
        val mNewTitle = SpannableString(mi.title)
        mNewTitle.setSpan(CustomTypefaceSpan("", Typeface.createFromAsset(assets, "fonts/Montserrat-Regular.otf")), 0, mNewTitle.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        mi.title = mNewTitle
    }
    object BottomNavigationViewHelper {
        @SuppressLint("RestrictedApi")
        fun disableShiftMode(view: BottomNavigationView) {
            val menuView = view.getChildAt(0) as BottomNavigationMenuView
            try {
                val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
                shiftingMode.isAccessible = true
                shiftingMode.setBoolean(menuView, false)
                shiftingMode.isAccessible = false
                for (i in 0 until menuView.childCount) {
                    val item = menuView.getChildAt(i) as BottomNavigationItemView

                    item.setShiftingMode(false)
                    // set once again checked value, so view will be updated

                    item.setChecked(item.itemData.isChecked)
                }
            } catch (e: NoSuchFieldException) {
                Log.e("BNVHelper", "Unable to get shift mode field", e)
            } catch (e: IllegalAccessException) {
                Log.e("BNVHelper", "Unable to change value of shift mode", e)
            }

        }
    }


    private var exit: Boolean = false
    override fun onBackPressed() {
        when {
            supportFragmentManager.backStackEntryCount > 1 -> {
                super.onBackPressed()
                toolbarTitle!!.text = supportFragmentManager.findFragmentById(R.id.framelayout).tag

            }
            supportFragmentManager.backStackEntryCount == 0 -> {
                if (exit) {
                    super.onBackPressed()
                    return
                }
                exit = true
                Toast.makeText(this, "Click again to exit", Toast.LENGTH_SHORT).show()
                Handler().postDelayed({ exit = false }, 2000)
            }
            else -> {
                super.onBackPressed()
                toolbarTitle!!.text = supportFragmentManager.findFragmentById(R.id.framelayout).tag
                hideBackArrow()
            }
        }
        if (supportFragmentManager.findFragmentById(R.id.framelayout).tag.equals(getString(R.string.title_toolbar_dashboard), true))
            imgSearch.visibility = View.VISIBLE
        else
            imgSearch.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)

    }
}
