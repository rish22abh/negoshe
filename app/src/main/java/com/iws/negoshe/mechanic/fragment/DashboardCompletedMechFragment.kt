package com.iws.negoshe.mechanic.fragment


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.BidDetailActivity
import com.iws.negoshe.mechanic.inerfacePkg.FragmentCommunicationMech
import com.iws.negoshe.user.DialogPkg.DialogFeedback
import com.iws.negoshe.user.model.CompleteJobBean
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_dashboard_pending.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardPendingFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardCompletedMechFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var latestJobAdapter: LatestJobAdapter? = null
    private var fragmentCommunication: FragmentCommunicationMech? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var listLatestJobBean: ArrayList<CompleteJobBean>? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentCommunication = context as FragmentCommunicationMech
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_pending, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DashboardPendingFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DashboardCompletedMechFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        listLatestJobBean = ArrayList()
        latestJobAdapter = LatestJobAdapter()
        recyclerViewPending.layoutManager = LinearLayoutManager(activity)
//        recyclerViewLatest.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerViewPending.adapter = latestJobAdapter
    }


    inner class LatestJobAdapter : RecyclerView.Adapter<LatestJobAdapter.MyViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard_pending, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {
//            return if (listDistributorListBean!!.size > 0)
//                listDistributorListBean!![0].data.size
//            else
            return listLatestJobBean!!.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.bind(position)
            holder.textTitle!!.text = listLatestJobBean!![position].job_title
            holder.textAssign!!.text = "$ ${listLatestJobBean!![position].job_amount}"
            holder.textAmount!!.text = Html.fromHtml("Amount paid- <b>$ " + listLatestJobBean!![position].job_amount + "</b>")
            holder.textAssign!!.text = Html.fromHtml("Assigned by <b>"+listLatestJobBean!![position].first_name + " " + listLatestJobBean!![position].last_name+ "</b>")
            Picasso.with(activity!!).load(listLatestJobBean!![position].profile_pic)
                    .placeholder(R.drawable.ic_user).into(holder.profile_image)
            holder.rating!!.progress = listLatestJobBean!![position].job_rating!!
            holder.textBidDetail!!.text = "Feedback Detail"
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(position: Int) {
                textBidDetail!!.setOnClickListener {
                    val dialogFeedback: com.iws.negoshe.user.DialogPkg.DialogFeedback = DialogFeedback(activity!!, 1)
                    dialogFeedback.setCancelable(false)
                    dialogFeedback.setRating(listLatestJobBean!![position].job_rating!!)
                    dialogFeedback.setJobNature(listLatestJobBean!![position].job_nature)
                    dialogFeedback.setImage(listLatestJobBean!![position].profile_pic)
                    dialogFeedback.setFeedbackComment(listLatestJobBean!![position].job_feedback!!)
                    dialogFeedback.setName(listLatestJobBean!![position].first_name + " " + listLatestJobBean!![position].last_name)
                    dialogFeedback.show()

                }
                textDetail!!.setOnClickListener {
                    val intent = Intent(activity!!, BidDetailActivity::class.java)
                    intent.putExtra("title", listLatestJobBean!![position].job_title)
                    intent.putExtra("image", listLatestJobBean!![position].job_image)
                    startActivity(intent)
                }
                if (listLatestJobBean!![position].job_image.equals("media/Pictures/job.png"))
                    textDetail!!.visibility = View.INVISIBLE
                else
                    textDetail!!.visibility = View.VISIBLE
                textTitle!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textAssign!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textAmount!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textBidDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
            }

            var profile_image: CircleImageView? = itemView.findViewById(R.id.profile_image)
            var textTitle: TextView? = itemView.findViewById(R.id.textTitle)
            var textBidDetail: TextView? = itemView.findViewById(R.id.textBidDetail)
            var textDetail: TextView? = itemView.findViewById(R.id.textDetail)
            var textAssign: TextView? = itemView.findViewById(R.id.textAssign)
            var textAmount: TextView? = itemView.findViewById(R.id.textAmount)
            var rating: RatingBar? = itemView.findViewById(R.id.rating)
        }
    }

    //Add Dynamic fragment to framelayout
    private fun addFragment(fragment: Fragment, title: String) {
        val fragmentManager = activity!!.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayout, fragment, title)
        fragmentTransaction.addToBackStack(title)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onResume() {
        super.onResume()
        onClickgetLatest(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
    }

    private fun onClickgetLatest(username: String, userFlag: String) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.complete_job_list(username, userFlag)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (!response.body().toString().equals("null")) {
                    val intStatus: Int = response.body()!!.get("status").asInt
                    listLatestJobBean!!.clear()
                    if (intStatus == 1) {
                        val arrayData: JsonArray = response.body()!!.get("message").asJsonArray
                        if (arrayData.size() > 0) {
                            for (i in 0 until arrayData.size()) {
                                val objectData: JsonObject = arrayData.get(i).asJsonObject
                                val latestJobBean = CompleteJobBean()
                                latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                }
                                latestJobBean.mechanic_profile = when (objectData.get("mechanic_profile").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("mechanic_profile").asString
                                }
                                latestJobBean.job_amount = when (objectData.get("job_amount").isJsonNull) {true -> 0
                                    false -> objectData.get("job_amount").asInt
                                }
                                latestJobBean.job_id = when (objectData.get("job_id").isJsonNull) {true -> 0
                                    false -> objectData.get("job_id").asInt
                                }
                                latestJobBean.job_rating = when (objectData.get("job_rating").isJsonNull) {true -> 0
                                    false -> objectData.get("job_rating").asInt
                                }
                                latestJobBean.job_title = when (objectData.get("job_title").isJsonNull) {true -> ""
                                    false -> objectData.get("job_title").asString
                                }
                                latestJobBean.first_name = when (objectData.get("first_name").isJsonNull) {true -> ""
                                    false -> objectData.get("first_name").asString
                                }
                                latestJobBean.last_name = when (objectData.get("last_name").isJsonNull) {true -> ""
                                    false -> objectData.get("last_name").asString
                                }
                                latestJobBean.job_feedback = when (objectData.get("job_feedback").isJsonNull) {true -> ""
                                    false -> objectData.get("job_feedback").asString
                                }
                                latestJobBean.job_nature = when (objectData.get("job_nature").isJsonNull) {true -> ""
                                    false -> objectData.get("job_nature").asString
                                }
                                latestJobBean.job_image = when (objectData.get("job_image").isJsonNull) {true -> ""
                                    false -> objectData.get("job_image").asString
                                }
                                listLatestJobBean!!.add(latestJobBean)
                            }
                        } else {
                            Toast.makeText(activity!!, "" + "No Completed Jobs.", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        if (activity != null)
                            if (response.body()!!.has("message"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                    }
                    latestJobAdapter!!.notifyDataSetChanged()
                }
            }
        })
    }
}
