package com.iws.negoshe.mechanic.fragment


import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.BidDetailActivity
import com.iws.negoshe.mechanic.model.LatestJobBean
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_dashboard_latest.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardLatestFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardLatestFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var latestJobAdapter: LatestJobAdapter? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var listLatestJobBean: ArrayList<LatestJobBean>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_latest, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DashboardLatestFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        listLatestJobBean = ArrayList()
        latestJobAdapter = LatestJobAdapter()
        recyclerViewLatest.layoutManager = LinearLayoutManager(activity)
//        recyclerViewLatest.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerViewLatest.adapter = latestJobAdapter
    }


    inner class LatestJobAdapter : RecyclerView.Adapter<LatestJobAdapter.MyViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard_latest, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {
//            return if (listDistributorListBean!!.size > 0)
//                listDistributorListBean!![0].data.size
//            else
            return listLatestJobBean!!.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.textTitle!!.text = listLatestJobBean!![position].job_title
            holder.textName!!.text = listLatestJobBean!![position].first_name
            holder.textEmail!!.text = listLatestJobBean!![position].email
            holder.textNumber!!.text = listLatestJobBean!![position].mobile

            if (listLatestJobBean!![position].job_count == 1) {
                holder.buttonPlaceBid!!.visibility = View.GONE
            } else {
                holder.buttonPlaceBid!!.visibility = View.VISIBLE
            }

            holder.bind(position)
            Picasso.with(activity!!).load(listLatestJobBean!![position].profile_pic)
                    .placeholder(R.drawable.ic_user_mech).into(holder.profile_image)
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var buttonPlaceBid: Button? = itemView.findViewById(R.id.buttonPlaceBid)
            var profile_image: CircleImageView? = itemView.findViewById(R.id.profile_image)
            var textTitle: TextView? = itemView.findViewById(R.id.textTitle)
            var textName: TextView? = itemView.findViewById(R.id.textName)
            var textEmail: TextView? = itemView.findViewById(R.id.textEmail)
            var textNumber: TextView? = itemView.findViewById(R.id.textNumber)
            var textJobDetail: TextView? = itemView.findViewById(R.id.textJobDetail)

            fun bind(position: Int) {
                buttonPlaceBid!!.setOnClickListener {
                    addFragment(PlaceBidFragment.newInstance(listLatestJobBean!![position].job_id.toString(), ""), listLatestJobBean!![position].job_title!!)
                }
                textJobDetail!!.setOnClickListener {
                    val intent = Intent(activity!!, BidDetailActivity::class.java)
//                    val bundle = Bundle()
//                    bundle.putParcelable("bundle", listLatestJobBean!![0].message[position])
                    intent.putExtra("title", listLatestJobBean!![position].job_title)
                    intent.putExtra("image", listLatestJobBean!![position].job_img)
                    startActivity(intent)
                }
                if (listLatestJobBean!![position].job_img.equals("media/Pictures/job.png"))
                    textJobDetail!!.visibility = View.GONE
                else
                    textJobDetail!!.visibility = View.VISIBLE
                textTitle!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textName!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textEmail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textNumber!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                buttonPlaceBid!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
                textJobDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
            }
        }
    }

    //Add Dynamic fragment to framelayout
    private fun addFragment(fragment: Fragment, title: String) {
        val fragmentManager = activity!!.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayout, fragment, title)
        fragmentTransaction.addToBackStack(title)
        fragmentTransaction.commitAllowingStateLoss()
        activity!!.findViewById<TextView>(R.id.toolbarTitle).text = title
    }

    override fun onResume() {
        super.onResume()
        onClickgetLatest(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
    }

    private fun onClickgetLatest(username: String, userFlag: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("user_flag", userFlag)
        jsonObject.addProperty("job_nature", sharedprferencesDetails!!.getString(SharedprferencesDetails.job_nature))
        latestJob(jsonObject)

    }

    fun latestJob(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.mechanic_job_search(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                if (activity != null)
                    when (t) {
                        is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body()!!.toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        listLatestJobBean!!.clear()
                        if (intStatus == 1) {
                            val arrayData: JsonArray = response.body()!!.get("message").asJsonArray
                            for (i in 0 until arrayData.size()) {
                                val objectData: JsonObject = arrayData.get(i).asJsonObject
                                val latestJobBean = LatestJobBean()
                                latestJobBean.job_count = when (objectData.get("job_count").isJsonNull) {true -> 0
                                    false -> objectData.get("job_count").asInt
                                }
                                latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                }
                                latestJobBean.first_name = when (objectData.get("first_name").isJsonNull) {true -> ""
                                    false -> objectData.get("first_name").asString
                                }
                                latestJobBean.job_id = when (objectData.get("job_id").isJsonNull) {true -> 0
                                    false -> objectData.get("job_id").asInt
                                }
                                latestJobBean.mobile = when (objectData.get("mobile").isJsonNull) {true -> ""
                                    false -> objectData.get("mobile").asString
                                }
                                latestJobBean.email = when (objectData.get("email").isJsonNull) {true -> ""
                                    false -> objectData.get("email").asString
                                }
                                latestJobBean.job_title = when (objectData.get("job_title").isJsonNull) {true -> ""
                                    false -> objectData.get("job_title").asString
                                }
                                latestJobBean.job_img = when (objectData.get("job_image").isJsonNull) {true -> ""
                                    false -> objectData.get("job_image").asString
                                }
                                listLatestJobBean!!.add(latestJobBean)
                            }
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                        latestJobAdapter!!.notifyDataSetChanged()
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
