package com.iws.negoshe.mechanic.fragment


import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_dashboard_mechanic.*


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_mechanic, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DashboardFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addFragment(DashboardLatestFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))

        textLatest.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textOngoing.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textCompleted.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")

        constraintLatestJob.setOnClickListener {
            addFragment(DashboardLatestFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewOngoing.visibility = View.GONE
            viewCompleted.visibility = View.GONE
            viewLatest.visibility = View.VISIBLE
            textLatest.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimaryDarkMechanic))
            textOngoing.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
//            imageOngoing.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ongoing_job_unselected))
//            imageLatest.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.latest_job_selected))
        }
        constraintOngoingJob.setOnClickListener {
            addFragment(DashboardOngoingFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewOngoing.visibility = View.VISIBLE
            viewLatest.visibility = View.GONE
            viewCompleted.visibility = View.GONE
            textOngoing.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimaryDarkMechanic))
            textLatest.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
//            imageOngoing.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ongoing_job_selected))
//            imageLatest.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.latest_job_unselected))
        }
        constraintCompleted.setOnClickListener {
            addFragment(DashboardCompletedMechFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewOngoing.visibility = View.GONE
            viewCompleted.visibility = View.VISIBLE
            viewLatest.visibility = View.GONE
            textOngoing.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
            textLatest.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentMechanic))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorPrimaryDarkMechanic))
//            imageOngoing.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ongoing_job_selected))
//            imageLatest.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.latest_job_unselected))
        }
    }

    //Add Dynamic fragment to framelayout
    private fun addFragment(fragment: Fragment, title: String) {
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayoutChild, fragment, title)
        fragmentTransaction.commitAllowingStateLoss()
    }
}
