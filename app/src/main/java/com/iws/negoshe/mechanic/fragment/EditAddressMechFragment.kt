package com.iws.negoshe.mechanic.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_edit_address_mech.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditAddressFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class EditAddressMechFragment : Fragment(), OnMapReadyCallback {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var googleMap: GoogleMap? = null

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        var sydney: LatLng? = null
        sydney = if (!param1!!.equals("")) {
            try {
                LatLng(getLocationFromAddress(param1!!)!!.latitude, getLocationFromAddress(param1!!)!!.longitude)
            } catch (e: Exception) {
                LatLng(0.0, 0.0)
            }
        } else
            LatLng(0.0, 0.0)
        googleMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(sydney, 15f))

        googleMap.setOnCameraMoveStartedListener {
            val centerOfMap: LatLng = googleMap.cameraPosition.target
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(centerOfMap))

            googleMap.setOnCameraIdleListener {
                try {
                    val geocoder = Geocoder(activity, Locale.getDefault())
                    val addresses: List<Address>

                    addresses = geocoder.getFromLocation(centerOfMap.latitude, centerOfMap.longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses.isNotEmpty()) {
                        val address: String = addresses.get(0).getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        inputAddress.setText(address)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(activity!!, "Internet not working properly!!", Toast.LENGTH_SHORT).show()
                }
            }


        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

    }

    fun getLocationFromAddress(strAddress: String): Address? {

        val coder = Geocoder(activity)
        val address: List<Address>

        try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location: Address = address.get(0)
            location.latitude
            location.longitude
            return location
        } catch (e: Exception) {
            return null
        }
    }

    private var param1: String? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_address_mech, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditAddressFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                EditAddressMechFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    internal var mapFragment: SupportMapFragment? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        inputAddress.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
        inputAddress.setText(param1)
        buttonSubmit.setOnClickListener {
            hideKeyboard()
            if (!inputAddress.text.toString().isEmpty()) {
                val address: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputAddress.text.toString())
                val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email))

                updateProfile(username, address)
            } else {
                Toast.makeText(activity, "Enter Address.", Toast.LENGTH_SHORT).show()
            }
        }

        imgGPS!!.setOnClickListener {
            checkPermissionGranted()
        }

        Handler().postDelayed(Runnable {
            {

            }
            mapFragment = childFragmentManager
                    .findFragmentById(R.id.map) as SupportMapFragment

            mapFragment!!.getMapAsync(this)
        }, 500)
        inputAddress.isLongClickable = false
        inputAddress.customSelectionActionModeCallback = object : ActionMode.Callback {
            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                return false; }

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }

        }

    }

    fun hideKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(inputAddress.windowToken, 0)
    }

    fun updateProfile(userName: RequestBody, website: RequestBody) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.update_profileAddress(userName, website)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "update_profile-${t!!.message}")
                progressDialog.dismiss()
                if (activity != null)
                    when (t) {
                        is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "update_profile-${t!!.body().toString()}"+t.code())
                progressDialog.dismiss()
                try {
                    if (t.code() == 200||t.code() == 201||t.code() == 202) {
                        if (activity != null)
                            if (t.body()!!.isJsonObject) {
                                val intStatus: Int = t.body()!!.get("status").asInt
                                if (intStatus == 1) {
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                    sharedprferencesDetails!!.putString(SharedprferencesDetails.address, inputAddress.text.toString())
//                        activity!!.onBackPressed()
                                    inputAddress.setText("")
//                                    activity!!.findViewById<BottomNavigationView>(R.id.navigation).menu.getItem(0).isEnabled = true
//                                    activity!!.findViewById<BottomNavigationView>(R.id.navigation).menu.getItem(1).isEnabled = true
//                                    activity!!.findViewById<BottomNavigationView>(R.id.navigation).menu.getItem(2).isEnabled = true
//                                    activity!!.findViewById<BottomNavigationView>(R.id.navigation).menu.getItem(3).isEnabled = true
                                    activity!!.onBackPressed()
                                } else {
                                    if (t.body()!!.has("message"))
                                        Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                    else if (t.body()!!.has("data"))
                                        Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                                }
                            }
                    } else {
                        Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                }
            }
        })
    }

    private val PERMISSIONS_REQUEST_LOCATION = 1

    private fun checkPermissionGranted() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                            Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(activity!!, "To create job you need to give Location permission from settings.", Toast.LENGTH_SHORT).show()
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSIONS_REQUEST_LOCATION)
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(activity!!,
                        arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSIONS_REQUEST_LOCATION)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            createJobAfterPermission()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    createJobAfterPermission()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(activity!!, "To update address you need to give Location permission.", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun createJobAfterPermission() {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        val geocoder = Geocoder(activity, Locale.getDefault())
                        val addresses: List<Address>

                        addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                        val address: String = addresses.get(0).getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                        Toast.makeText(activity!!, "" + address, Toast.LENGTH_SHORT).show()

                        googleMap!!.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(LatLng(location.latitude, location.longitude), 15f))
                        Handler().postDelayed(Runnable {
                            inputAddress.setText(address)
                        }, 500)

                    }
                }
    }
}
