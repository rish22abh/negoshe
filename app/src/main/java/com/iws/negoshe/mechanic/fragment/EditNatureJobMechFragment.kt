package com.iws.negoshe.mechanic.fragment

import android.app.ProgressDialog
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.model.request.GetJobSkillBean
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_edit_naturejob_mech.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditMobileFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class EditNatureJobMechFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var spinnerText: String? = ""
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var customJobSkillAdapter: CustomJobSkillAdapter? = null
    private var listGetJobSkillBean: ArrayList<GetJobSkillBean>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_naturejob_mech, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditMobileFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                EditNatureJobMechFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listGetJobSkillBean = ArrayList()
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)


        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
        buttonSubmit.setOnClickListener {
            val website: RequestBody = RequestBody.create(MediaType.parse("text/plain"), spinnerText!!)
            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.username))
            updateProfile(username, website)
        }

        customJobSkillAdapter = CustomJobSkillAdapter()
        spinnerJobtype.adapter = customJobSkillAdapter
        spinnerJobtype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                spinnerText = listGetJobSkillBean!![0].message[position]
            }
        }
    }

    fun updateProfile(userName: RequestBody, job_nature: RequestBody) {
        val progressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.update_profilejob_nature(userName, job_nature)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "update_profile-${t!!.message}")
                progressDialog.dismiss()
                if (activity != null)
                    when (t) {
                        is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "update_profile-${t!!.body().toString()}")
                progressDialog.dismiss()
                try {
                    if (t.code() == 200||t.code() == 201||t.code() == 202) {
                        if (activity != null)
                        if (t.body()!!.isJsonObject) {
                            val intStatus: Int = t.body()!!.get("status").asInt
                            if (intStatus == 1) {
                                Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                sharedprferencesDetails!!.putString(SharedprferencesDetails.job_nature, spinnerText!!)
                                activity!!.onBackPressed()
                            } else {
                                if (t.body()!!.has("message"))
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (t.body()!!.has("data"))
                                    Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
                } catch (e: Exception) {

                }
            }
        })
    }


    override fun onResume() {
        super.onResume()
        getJobType()
    }


    fun getJobType() {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.get_skill_list()
        call.enqueue(object : Callback<GetJobSkillBean> {
            override fun onFailure(call: Call<GetJobSkillBean>?, t: Throwable?) {
                Log.e("response_onFailure", "getJobType-${t!!.message}")
                progressDialog.dismiss()
                if (!call!!.isCanceled) {

                    when (t) {
                        is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: Call<GetJobSkillBean>?, t: Response<GetJobSkillBean>?) {
//                Log.e("response_onResponse", "getJobType-${t!!.body()!!.message}")
                progressDialog.dismiss()
                if (t!!.code() == 200||t.code() == 201||t.code() == 202) {
                    if (!t.body()!!.equals(null)) {
                        val intStatus: Int = t.body()!!.status
                        if (intStatus == 1) {
                            listGetJobSkillBean!!.add(t.body()!!)
                            customJobSkillAdapter!!.notifyDataSetChanged()
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }

            }
        })
    }


    private inner class CustomJobSkillAdapter : BaseAdapter() {


        override fun getCount(): Int {
            return if (listGetJobSkillBean!!.size > 0)
                listGetJobSkillBean!![0].message.size
            else
                0
        }

        override fun getItem(position: Int): Any {
            return listGetJobSkillBean!![0].message[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            val cur_obj = listGetJobSkillBean!![0].message[position]
            val inflater = activity!!.layoutInflater
            val row = inflater.inflate(android.R.layout.simple_spinner_item, parent, false)
            val label: TextView = row.findViewById(android.R.id.text1)
            label.text = cur_obj
            row.setPadding(0, row.paddingTop, row.paddingRight, row.paddingBottom)

            return row
        }

        override fun getDropDownView(position: Int, convertView: View?,
                                     parent: ViewGroup): View {
            val cur_obj = listGetJobSkillBean!![0].message[position]
            val inflater = activity!!.layoutInflater
            val dropDownView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)

            val dd_GoText: TextView = dropDownView.findViewById(android.R.id.text1)
            dd_GoText.text = cur_obj

            return dropDownView

        }
    }
}
