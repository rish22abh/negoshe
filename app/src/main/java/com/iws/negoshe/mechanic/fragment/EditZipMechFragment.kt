package com.iws.negoshe.mechanic.fragment


import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_edit_zip_mech.*
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [EditZipFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class EditZipMechFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_zip_mech, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment EditZipFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                EditZipMechFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)

        inputWebsite.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
        inputWebsite.setText(param1)
        buttonSubmit.setOnClickListener {
            hideKeyboard()
            if (!inputWebsite.text.toString().isEmpty()) {
                val website: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputWebsite.text.toString())
                val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email))

                updateProfile(username, website)
            } else {
                Toast.makeText(activity, "Enter Zip code.", Toast.LENGTH_SHORT).show()

            }

        }
    }

    fun hideKeyboard() {

        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(inputWebsite.windowToken, 0)

    }

    fun updateProfile(userName: RequestBody, website: RequestBody) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.update_profileZip(userName, website)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "update_profile-${t!!.message}")
                progressDialog.dismiss()
                if (activity != null)
                    if (!call!!.isCanceled) {
                        when (t) {
                            is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                            is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                            is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        }
                    }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "update_profile-${t!!.body().toString()}")
                progressDialog.dismiss()
                try {
                    if (t.code() == 200||t.code() == 201||t.code() == 202) {
                        if (activity != null) {
                            if (t.body()!!.isJsonObject) {
                                val intStatus: Int = t.body()!!.get("status").asInt
                                if (intStatus == 1) {
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                    sharedprferencesDetails!!.putString(SharedprferencesDetails.zipcode, inputWebsite.text.toString())
                                    inputWebsite.setText("")

                                    activity!!.onBackPressed()
                                } else {
                                    if (t.body()!!.has("message"))
                                        Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                    else if (t.body()!!.has("data"))
                                        Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                                }

                            }
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                }
            }
        })
    }
}
