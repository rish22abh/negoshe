package com.iws.negoshe.mechanic.fragment


import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails

import com.iws.negoshe.R
import com.iws.negoshe.mechanic.model.LatestJobBean
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_feedback_mech.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FeedbackMechFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FeedbackMechFragment : Fragment() {
    private var param1: LatestJobBean? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getParcelable(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feedback_mech, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: LatestJobBean, param2: String) =
                FeedbackMechFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails =SharedprferencesDetails.getInstance(activity!!)
        Picasso.with(activity).load(param1!!.profile_pic)
                .placeholder(R.drawable.ic_mechanic).into(imageViewFeedback)
        textName.text = param1!!.first_name
        textTitle.text = param1!!.job_title

        buttonSend.setOnClickListener {
            if (rating.rating == 0.0F)
                Toast.makeText(activity!!, "Please Rate This Job.", Toast.LENGTH_SHORT).show()
            else {
                val jsonObject = JsonObject()
                jsonObject.addProperty("job_id", param1!!.job_id)
                jsonObject.addProperty("job_rating", rating.rating)
                jsonObject.addProperty("job_feedback", edtComment.text.toString())
                jsonObject.addProperty("user_flag", sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
                feedback(jsonObject)
            }
        }
    }


    fun feedback(feedback: JsonObject) {
        val progressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.job_feedback(feedback)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}" + response.code())
                progressDialog.dismiss()
                if (response.code() == 200||response.code() == 201||response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
//                            onClickgetLatest(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
                            Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            activity!!.onBackPressed()
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
