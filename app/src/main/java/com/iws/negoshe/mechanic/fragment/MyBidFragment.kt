package com.iws.negoshe.mechanic.fragment


import android.app.ProgressDialog
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.mechanic.model.MyBidBean
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_my_bid.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MyBidFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MyBidFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var latestJobAdapter: LatestJobAdapter? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var listLatestJobBean: ArrayList<MyBidBean>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_bid, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MyBidFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MyBidFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        listLatestJobBean = ArrayList()
        latestJobAdapter = LatestJobAdapter()
        recyclerViewMyBid.layoutManager = LinearLayoutManager(activity)
//        recyclerViewLatest.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerViewMyBid.adapter = latestJobAdapter
    }

    override fun onResume() {
        super.onResume()
        getMyBidList()
    }

    private fun getMyBidList() {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.bid_list(sharedprferencesDetails!!.getString(SharedprferencesDetails.user_id))
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (!response.body().toString().equals("null")) {
                    val intStatus: Int = response.body()!!.get("status").asInt
                    listLatestJobBean!!.clear()
                    if (intStatus == 1) {
                        val arrayData: JsonArray = response.body()!!.get("message").asJsonArray
                        if (arrayData.size() > 0) {
                            for (i in 0 until arrayData.size()) {
                                val objectData: JsonObject = arrayData.get(i).asJsonObject
                                val latestJobBean = MyBidBean()
                                latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                }
                                latestJobBean.first_name = when (objectData.get("first_name").isJsonNull) {true -> ""
                                    false -> objectData.get("first_name").asString
                                }
                                latestJobBean.last_name = when (objectData.get("last_name").isJsonNull) {true -> ""
                                    false -> objectData.get("last_name").asString
                                }
                                latestJobBean.bid_amount = when (objectData.get("bid_amount").isJsonNull) {true -> 0
                                    false -> objectData.get("bid_amount").asInt
                                }
                                latestJobBean.bid_added = when (objectData.get("bid_added").isJsonNull) {true -> ""
                                    false -> objectData.get("bid_added").asString
                                }
                                latestJobBean.bid_id = when (objectData.get("bid_id").isJsonNull) {true -> 0
                                    false -> objectData.get("bid_id").asInt
                                }
                                latestJobBean.address = when (objectData.get("address").isJsonNull) {true -> ""
                                    false -> objectData.get("address").asString
                                }
                                latestJobBean.bid_image = when (objectData.get("bid_image").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_ImagePost + objectData.get("bid_image").asString
                                }
                                latestJobBean.work_hours = when (objectData.get("work_hours").isJsonNull) {true -> 0
                                    false -> objectData.get("work_hours").asInt
                                }
                                latestJobBean.job_title = when (objectData.get("job_title").isJsonNull) {true -> ""
                                    false -> objectData.get("job_title").asString
                                }
                                listLatestJobBean!!.add(latestJobBean)
                            }
                        } else {
                            Toast.makeText(activity!!, "" + "No Bid Placed.", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        if (activity != null)
                            if (response.body()!!.has("message"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                    }
                    latestJobAdapter!!.notifyDataSetChanged()
                }
            }
        })
    }


    inner class LatestJobAdapter : RecyclerView.Adapter<LatestJobAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_mybid, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {
            return listLatestJobBean!!.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.textClientName.text = listLatestJobBean!!.get(position).first_name + " " + listLatestJobBean!!.get(position).last_name
            holder.textDateIssued.text = convertDate(listLatestJobBean!![position].bid_added!!)
            holder.textDate.text = convertDateFull(listLatestJobBean!!.get(position).bid_added!!)
            holder.textDueDate.text = convertDateDue(listLatestJobBean!!.get(position).bid_added!!, listLatestJobBean!!.get(position).work_hours!!)
            holder.textInvoiceno.text = listLatestJobBean!!.get(position).bid_id!!.toString()
            holder.textMechName.text = sharedprferencesDetails!!.getString(SharedprferencesDetails.first_name) + " " + sharedprferencesDetails!!.getString(SharedprferencesDetails.last_name)
            holder.textAccount.text = sharedprferencesDetails!!.getString(SharedprferencesDetails.first_name) + " " + sharedprferencesDetails!!.getString(SharedprferencesDetails.last_name)
            holder.textMechAddress.text = sharedprferencesDetails!!.getString(SharedprferencesDetails.address)
            holder.textZIp.text = sharedprferencesDetails!!.getString(SharedprferencesDetails.zipcode)
            holder.textJobTitle.text = listLatestJobBean!![position].job_title
            holder.textJobRate.text = "$ " + DecimalFormat("##.##").format(listLatestJobBean!![position].bid_amount!!.toFloat() / listLatestJobBean!!.get(position).work_hours!!)
            holder.textJobWork.text = listLatestJobBean!!.get(position).work_hours!!.toString()
            holder.textTitle.text = listLatestJobBean!!.get(position).job_title
            holder.textAmount.text = "$ " + listLatestJobBean!![position].bid_amount
            holder.textSubTotal.text = "$ " + DecimalFormat("##.00").format((listLatestJobBean!![position].bid_amount!!)).toString()
            holder.textTotalDue.text = "$ " + DecimalFormat("##.00").format((listLatestJobBean!![position].bid_amount!!)).toString()

            Picasso.with(activity!!).load(listLatestJobBean!!.get(position).profile_pic).error(R.drawable.ic_mechanic)
                    .placeholder(R.drawable.ic_mechanic).into(holder.imagePost)
            holder.bind(position)
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(position: Int) {
                itemView.setOnClickListener {
                    if (cardViewCollapsed.visibility == View.VISIBLE) {
                        cardViewExpand.visibility = View.VISIBLE
                        cardViewCollapsed.visibility = View.GONE
                    } else {
                        cardViewCollapsed.visibility = View.VISIBLE
                        cardViewExpand.visibility = View.GONE
                    }
                }
                textTitle.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textAmount.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textDate.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textClientName.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textDateHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                textDateIssued.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textInvoiceno.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textInvoicenoHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                textMechName.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textMechAddress.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textJobTitle.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textJobRate.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textJobWork.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textSubTotal.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textZIp.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textDueDate.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textDescriptionHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textRateHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textHoursHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textSubTotalHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textDueByHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textTotalHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textTotalDue.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
            }

            var cardViewExpand: CardView = itemView.findViewById(R.id.cardViewExpand)
            var cardViewCollapsed: CardView = itemView.findViewById(R.id.cardViewCollapsed)
            var textRateHeading: TextView = itemView.findViewById(R.id.textRateHeading)
            var textSubTotalHeading: TextView = itemView.findViewById(R.id.textSubTotalHeading)
            var textHoursHeading: TextView = itemView.findViewById(R.id.textHoursHeading)
            var textClientName: TextView = itemView.findViewById(R.id.textClientName)
            var textDateHeading: TextView = itemView.findViewById(R.id.textDateHeading)
            var textDateIssued: TextView = itemView.findViewById(R.id.textDateIssued)
            var textInvoicenoHeading: TextView = itemView.findViewById(R.id.textInvoicenoHeading)
            var textInvoiceno: TextView = itemView.findViewById(R.id.textInvoiceno)
            var textMechName: TextView = itemView.findViewById(R.id.textMechName)
            var textMechAddress: TextView = itemView.findViewById(R.id.textMechAddress)
            var textZIp: TextView = itemView.findViewById(R.id.textZIp)
            var textJobTitle: TextView = itemView.findViewById(R.id.textJobTitle)
            var textJobRate: TextView = itemView.findViewById(R.id.textJobRate)
            var textJobWork: TextView = itemView.findViewById(R.id.textJobWork)
            var textSubTotal: TextView = itemView.findViewById(R.id.textSubTotal)
            var textTotalDue: TextView = itemView.findViewById(R.id.textTotalDue)
            var textTitle: TextView = itemView.findViewById(R.id.textTitle)
            var textAmount: TextView = itemView.findViewById(R.id.textAmount)
            var textDate: TextView = itemView.findViewById(R.id.textDate)
            var textDueDate: TextView = itemView.findViewById(R.id.textDueDate)
            var textAccount: TextView = itemView.findViewById(R.id.textAccount)
            var textDescriptionHeading: TextView = itemView.findViewById(R.id.textDescriptionHeading)
            var textDueByHeading: TextView = itemView.findViewById(R.id.textDueByHeading)
            var textTotalHeading: TextView = itemView.findViewById(R.id.textTotalHeading)
            var imagePost: ImageView = itemView.findViewById(R.id.imagePost)
        }
    }

    private fun convertDateDue(bid_added: String, work_hours: Int): String? {
        val separated = bid_added.split("T")
//        separated[0]
//        separated[1]

        try {

            val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val outputFormat = SimpleDateFormat("dd MMM yy", Locale.getDefault())
            val date1 = inputFormat.parse(separated[0])
            val cal = Calendar.getInstance()
            cal.time = date1
            cal.add(Calendar.HOUR_OF_DAY, work_hours)
            return outputFormat.format(cal.time)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    fun convertDate(date: String): String {
        val separated = date.split("T")
//        separated[0]
//        separated[1]

        try {

            val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val outputFormat = SimpleDateFormat("dd MMM yy", Locale.getDefault())
            val date1 = inputFormat.parse(separated[0])
            return outputFormat.format(date1)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

    fun convertDateFull(date: String): String {
        val separated = date.split("T")
//        separated[0]
//        separated[1]

        try {

            val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val outputFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            val date1 = inputFormat.parse(separated[0])
            return outputFormat.format(date1)
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }
}
