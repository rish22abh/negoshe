package com.iws.negoshe.mechanic.fragment


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_place_bid.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.text.DateFormat
import java.util.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlaceBidFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PlaceBidFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var finalFile: File? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private val CAMERA_PERMISSION_CODE = 23

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_place_bid, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PlaceBidFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                PlaceBidFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)

        inputBidAmount.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        inputworkHr.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textReceipt.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")

        frame.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            CAMERA_PERMISSION_CODE)
                } else {
                    openDialog()
                }
            } else {
                openDialog()
            }
        }


        buttonSubmit.setOnClickListener {
            hideKeyboard()
            if (inputBidAmount.text.toString().equals("") || inputBidAmount.text.toString().isEmpty())
                Toast.makeText(activity, "" + "Enter Bid Amount", Toast.LENGTH_SHORT).show()
            else if (inputworkHr.text.toString().equals("0") || inputworkHr.text.toString().isEmpty())
                Toast.makeText(activity, "" + "Enter valid Work duration", Toast.LENGTH_SHORT).show()
            else {
                if (finalFile != null) {
                    val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile!!)
                    val quotation_image: MultipartBody.Part = MultipartBody.Part.createFormData("quotation_image", finalFile!!.name, reqFile)

                    val quote_amount: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputBidAmount.text.toString())
                    val work_hours: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputworkHr.text.toString())
                    val userName: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                    val job_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), param1!!)

                    create_bid(
                            job_id,
                            userName,
                            quote_amount,
                            work_hours,
                            quotation_image)
                } else {
                    val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), "")
                    val quotation_image: MultipartBody.Part = MultipartBody.Part.createFormData("quotation_image", "", reqFile)

                    val quote_amount: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputBidAmount.text.toString())
                    val work_hours: RequestBody = RequestBody.create(MediaType.parse("text/plain"), inputworkHr.text.toString())
                    val userName: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                    val job_id: RequestBody = RequestBody.create(MediaType.parse("text/plain"), param1!!)

                    create_bid(
                            job_id,
                            userName,
                            quote_amount,
                            work_hours,
                            quotation_image)
                }
            }

        }
        inputBidAmount.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
        inputworkHr.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
        textReceipt.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")

    }

    fun hideKeyboard() {

        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(buttonSubmit.windowToken, 0)

    }

    fun create_bid(job_id: RequestBody, userName: RequestBody, quote_amount: RequestBody, work_hours: RequestBody, quotation_image: MultipartBody.Part) {

        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.create_bid(job_id, userName, quote_amount, work_hours, quotation_image)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "update_profile-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "update_profile-${t!!.body().toString()}")
                progressDialog.dismiss()
                try {
                    if (t.code() == 200||t.code() == 201||t.code() == 202) {
                        if (t.body()!!.isJsonObject) {
                            val intStatus: Int = t.body()!!.get("status").asInt
                            if (intStatus == 1) {
                                Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                activity!!.onBackPressed()
                            } else {
                                if (t.body()!!.has("message"))
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (t.body()!!.has("data"))
                                    Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                }
            }
        })
    }


    private fun openDialog() {
        val dialog1 = Dialog(activity!!)
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.setContentView(R.layout.camera_dialog_box_mech)
        //        Button dialogButtonOK = (Button) dialog1.findViewById(R.id.ok_btn);
        val take_photo = dialog1.findViewById(R.id.textTakePhoto) as TextView
        val tv = dialog1.findViewById(R.id.textHeading) as TextView
        val gallery = dialog1.findViewById(R.id.textGallery) as TextView
        val cancel = dialog1.findViewById(R.id.textCancel) as TextView
        dialog1.show()


        take_photo.setOnClickListener {
            dialog1.dismiss()
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, 0)
        }

        gallery.setOnClickListener {
            dialog1.dismiss()
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT//
            startActivityForResult(Intent.createChooser(intent, "Select File"), 1)
        }

        cancel.setOnClickListener { dialog1.dismiss() }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_PERMISSION_CODE -> openDialog()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val image = data.extras!!.get("data") as Bitmap
                //                iv_toolbar_profile.setImageBitmap(image);

                /* val stream = ByteArrayOutputStream()
                 image.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                 val byteFormat = stream.toByteArray()
                 imgString = Base64.encodeToString(byteFormat, Base64.DEFAULT)

                 //                Uri uri = data.getData();*/
                val currentDateTimeString = DateFormat.getDateTimeInstance().format(Date())
                textReceipt.text = "img$currentDateTimeString"


                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                val tempUri = getImageUri(activity!!, image)

                Log.e("File", "file-" + getRealPathFromURI(tempUri))
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                finalFile = File(getRealPathFromURI(tempUri))

            }
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            var bm: Bitmap? = null
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data.data)
                    //                    iv_toolbar_profile.setImageBitmap(bm);

                    val uri = data.data
                    textReceipt.text = getRealPathFromURI(uri)
/*
                    val stream = ByteArrayOutputStream()
                    bm!!.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                    val byteFormat = stream.toByteArray()
                    imgString = Base64.encodeToString(byteFormat, Base64.DEFAULT)*/

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    val tempUri = getImageUri(activity!!, bm)

                    Log.e("File", "file-" + getRealPathFromURI(uri))
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    finalFile = File(getRealPathFromURI(uri))

                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    private fun getRealPathFromURI(contentURI: Uri): String {

        val cursor = activity!!.contentResolver.query(contentURI, null, null, null, null)
        cursor.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

}
