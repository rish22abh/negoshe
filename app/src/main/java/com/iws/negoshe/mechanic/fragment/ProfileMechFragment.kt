package com.iws.negoshe.mechanic.fragment


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.LoginActivity
import com.iws.negoshe.app.utils.AsteriskPasswordTransformationMethod
import com.iws.negoshe.mechanic.inerfacePkg.FragmentCommunicationMech
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile_mech.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileMechFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ProfileMechFragment : Fragment(), View.OnClickListener {

    private var finalFile: File? = null
    private val CAMERA_PERMISSION_CODE = 23
    private val GALLERY_PERMISSION_CODE = 24
    private val HARDWARECAMERA_PERMISSION_CODE = 0
    private var param1: String? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var fragmentCommunication: FragmentCommunicationMech? = null
    private var bannerInt: Int = 0

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentCommunication = context as FragmentCommunicationMech
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_mech, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileMechFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ProfileMechFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_pic)).placeholder(R.drawable.ic_mechanic).into(profile_image)
        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img1).equals(""))
            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img1))
                    .placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner1)
        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img2).equals(""))
            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img2))
                    .placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner2)
        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img3).equals(""))
            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img3))
                    .placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner3)
        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img4).equals(""))
            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img4))
                    .placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner4)


        textName.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textWalletText.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textwallet.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtEmail.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtMobile.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtwebsite.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtaddress.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtzip.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtPassword.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        edtnatureJob.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        buttonUpdate.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
        edtPassword!!.transformationMethod = AsteriskPasswordTransformationMethod()

        profile_image.setOnClickListener(this@ProfileMechFragment)
        textName.setOnClickListener(this@ProfileMechFragment)
        constraintProfile.setOnClickListener(this@ProfileMechFragment)
        edtMobile.setOnClickListener(this@ProfileMechFragment)
        edtwebsite.setOnClickListener(this@ProfileMechFragment)
        edtaddress.setOnClickListener(this@ProfileMechFragment)
        edtzip.setOnClickListener(this@ProfileMechFragment)
        edtPassword.setOnClickListener(this@ProfileMechFragment)
        imageBannerAdd1.setOnClickListener(this@ProfileMechFragment)
        imageBannerAdd2.setOnClickListener(this@ProfileMechFragment)
        imageBannerAdd3.setOnClickListener(this@ProfileMechFragment)
        imageBannerAdd4.setOnClickListener(this@ProfileMechFragment)
        buttonUpdate.setOnClickListener(this@ProfileMechFragment)
//        imgLogout.setOnClickListener(this@ProfileMechFragment)
        edtnatureJob.setOnClickListener(this@ProfileMechFragment)
    }

    override fun onResume() {
        super.onResume()
        textName.text = sharedprferencesDetails!!.getString(SharedprferencesDetails.first_name) + " " + sharedprferencesDetails!!.getString(SharedprferencesDetails.last_name)
        edtEmail.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.email))
        textwallet.text = "$ ${sharedprferencesDetails!!.getString(SharedprferencesDetails.wallet_amount)}"
        edtMobile.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.mobile))
        edtwebsite.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.website))
        edtaddress.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.address))
        edtzip.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.zipcode))
        edtPassword.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.password))
        edtnatureJob.setText(sharedprferencesDetails!!.getString(SharedprferencesDetails.job_nature))
        getJobType()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.edtnatureJob -> {
                val fragment = EditNatureJobMechFragment.newInstance(edtnatureJob.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, "Nature of Job")
            }
            R.id.edtMobile -> {
                val fragment = EditMobileMechFragment.newInstance(edtMobile.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, getString(R.string.title_toolbar_editMobile))
            }
            R.id.edtwebsite -> {
                val fragment = EditWebsiteMechFragment.newInstance(edtwebsite.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, getString(R.string.title_toolbar_editWebsite))
            }
            R.id.edtaddress -> {
                val fragment = EditAddressMechFragment.newInstance(edtaddress.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, getString(R.string.title_toolbar_editAddress))
            }
            R.id.edtzip -> {
                val fragment = EditZipMechFragment.newInstance(edtzip.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, getString(R.string.title_toolbar_editZip))
            }
            R.id.edtPassword -> {
                val fragment = ChangePasswordMechFragment.newInstance(edtzip.text.toString(), "")
                fragmentCommunication!!.changeFragment(fragment, "Change Password")
            }
            R.id.textName -> {
                val fragment = EditNameMechFragment.newInstance("", "")
                fragmentCommunication!!.changeFragment(fragment, getString(R.string.title_toolbar_editName))
            }

            R.id.buttonUpdate -> {
                onClickLogout()
            }

            R.id.profile_image -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                CAMERA_PERMISSION_CODE)
                    } else {
                        bannerInt = 5
                        openDialog()
                    }
                } else {
                    bannerInt = 5
                    openDialog()
                }
            }
            R.id.imageBannerAdd1 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                CAMERA_PERMISSION_CODE)
                    } else {
                        bannerInt = 1
                        openDialog()
                    }
                } else {
                    bannerInt = 1
                    openDialog()
                }
            }
            R.id.imageBannerAdd2 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                CAMERA_PERMISSION_CODE)
                    } else {
                        bannerInt = 2
                        openDialog()
                    }
                } else {
                    bannerInt = 2
                    openDialog()
                }
            }
            R.id.imageBannerAdd3 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                CAMERA_PERMISSION_CODE)
                    } else {
                        bannerInt = 3
                        openDialog()
                    }
                } else {
                    bannerInt = 3
                    openDialog()
                }
            }
            R.id.imageBannerAdd4 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                CAMERA_PERMISSION_CODE)
                    } else {
                        bannerInt = 4
                        openDialog()
                    }
                } else {
                    bannerInt = 4
                    openDialog()
                }
            }
        }
    }

    private fun onClickLogout() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", sharedprferencesDetails!!.getString(SharedprferencesDetails.username))
        Log.e("request_logout", "login-$jsonObject")
        logout(jsonObject)

    }

    fun logout(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.user_sign_out(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()

                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
//                        val objectData: JsonObject = response.body()!!.get("data").asJsonObject
                            val sharedPreferencesDetails: SharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
                            sharedPreferencesDetails.clear()
                            activity!!.finish()

                            startActivity(Intent(activity!!, LoginActivity::class.java))
                        } else {
                            if (response.body()!!.has("message"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    fun updateProfile(userName: RequestBody, profile_pic: MultipartBody.Part) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.update_banner1(userName, profile_pic)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "update_profile-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "update_profile-${t!!.body().toString()}")
                progressDialog.dismiss()
                try {
                    if (t.code() == 200 || t.code() == 201 || t.code() == 202) {
                        if (t.body()!!.isJsonObject) {
                            val intStatus: Int = t.body()!!.get("status").asInt
                            if (intStatus == 1) {
                                bannerInt = 0
                                Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                onClickLogin(sharedprferencesDetails!!.getString(SharedprferencesDetails.email), sharedprferencesDetails!!.getString(SharedprferencesDetails.password))
                            } else {
                                bannerInt = 0
                                if (t.body()!!.has("message"))
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (t.body()!!.has("data"))
                                    Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                }
            }
        })
    }

    private fun onClickLogin(username: String, password: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)
        jsonObject.addProperty("push_token", sharedprferencesDetails!!.getString(SharedprferencesDetails.registrationId))
        login(jsonObject)

    }

    fun login(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.user_login(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    val intStatus: Int = response.body()!!.get("status").asInt
                    if (intStatus == 1) {
                        val objectData: JsonObject = response.body()!!.get("data").asJsonObject
//                    val sharedPreferencesDetails: SharedprferencesDetails = SharedprferencesDetails.getInstance(activity)
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.website, when (objectData.get("website").isJsonNull) {true -> ""
                            false -> objectData.get("website").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.username, when (objectData.get("username").isJsonNull) {true -> ""
                            false -> objectData.get("username").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.last_name, when (objectData.get("last_name").isJsonNull) {true -> ""
                            false -> objectData.get("last_name").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.profile_pic, when (objectData.get("profile_pic").isJsonNull) {true -> ""
                            false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.is_active, when (objectData.get("is_active").isJsonNull) {true -> ""
                            false -> objectData.get("is_active").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.profile_owner, when (objectData.get("profile_owner").isJsonNull) {true -> ""
                            false -> objectData.get("profile_owner").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.address, when (objectData.get("address").isJsonNull) {true -> ""
                            false -> objectData.get("address").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.first_name, when (objectData.get("first_name").isJsonNull) {true -> ""
                            false -> objectData.get("first_name").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.job_nature, when (objectData.get("job_nature").isJsonNull) {true -> ""
                            false -> objectData.get("job_nature").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.mobile, when (objectData.get("mobile").isJsonNull) {true -> ""
                            false -> objectData.get("mobile").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.wallet_amount, when (objectData.get("wallet_amount").isJsonNull) {true -> ""
                            false -> objectData.get("wallet_amount").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.zipcode, when (objectData.get("zipcode").isJsonNull) {true -> ""
                            false -> objectData.get("zipcode").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.email, when (objectData.get("email").isJsonNull) {true -> ""
                            false -> objectData.get("email").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.user_id, when (objectData.get("user_id").isJsonNull) {true -> ""
                            false -> objectData.get("user_id").asString
                        })

                        sharedprferencesDetails!!.putString(SharedprferencesDetails.banner_img1, when (objectData.get("banner_img1").isJsonNull) {true -> ""
                            false -> ApiClient.BASE_URL_Image + objectData.get("banner_img1").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.banner_img3, when (objectData.get("banner_img3").isJsonNull) {true -> ""
                            false -> ApiClient.BASE_URL_Image + objectData.get("banner_img3").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.banner_img4, when (objectData.get("banner_img4").isJsonNull) {true -> ""
                            false -> ApiClient.BASE_URL_Image + objectData.get("banner_img4").asString
                        })
                        sharedprferencesDetails!!.putString(SharedprferencesDetails.banner_img2, when (objectData.get("banner_img2").isJsonNull) {true -> ""
                            false -> ApiClient.BASE_URL_Image + objectData.get("banner_img2").asString
                        })
                        onResume()
                        Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_pic)).placeholder(R.drawable.ic_mechanic).error(R.drawable.ic_mechanic).into(profile_image)
                        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img1).equals(""))
                            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img1)).placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner1)
                        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img2).equals(""))
                            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img2)).placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner2)
                        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img3).equals(""))
                            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img3)).placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner3)
                        if (!sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img4).equals(""))
                            Picasso.with(activity).load(sharedprferencesDetails!!.getString(SharedprferencesDetails.banner_img4)).placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into(imageBanner4)
                    } else {
                        if (response.body()!!.has("message"))
                            Toast.makeText(activity, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                        else if (response.body()!!.has("data"))
                            Toast.makeText(activity, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(activity!!, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun openDialog() {
        val dialog1 = Dialog(activity!!)
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.setContentView(R.layout.camera_dialog_box_mech)
        //        Button dialogButtonOK = (Button) dialog1.findViewById(R.id.ok_btn);
        val take_photo = dialog1.findViewById(R.id.textTakePhoto) as TextView
        val tv = dialog1.findViewById(R.id.textHeading) as TextView
        val gallery = dialog1.findViewById(R.id.textGallery) as TextView
        val cancel = dialog1.findViewById(R.id.textCancel) as TextView
        dialog1.show()


        take_photo.setOnClickListener {
            dialog1.dismiss()
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, HARDWARECAMERA_PERMISSION_CODE)
        }

        gallery.setOnClickListener {
            dialog1.dismiss()
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT//
            startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_PERMISSION_CODE)
        }

        cancel.setOnClickListener { dialog1.dismiss() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == HARDWARECAMERA_PERMISSION_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val image = data.extras!!.get("data") as Bitmap
                if (bannerInt == 1)
                    imageBanner1.setImageBitmap(image)
                if (bannerInt == 2)
                    imageBanner2.setImageBitmap(image)
                if (bannerInt == 3)
                    imageBanner3.setImageBitmap(image)
                if (bannerInt == 4)
                    imageBanner4.setImageBitmap(image)
                if (bannerInt == 5)
                    profile_image.setImageBitmap(image)

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                val tempUri = getImageUri(activity!!, image)

                Log.e("File", "file-" + getRealPathFromURI(tempUri))
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                finalFile = File(getRealPathFromURI(tempUri))
                if (finalFile != null) {
                    if (bannerInt == 1) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                        val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img1", finalFile!!.name, reqFile)
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                        Handler().postDelayed(Runnable {
                            updateProfile(
                                    username,
                                    profile_pic)
                        }, 1000)
                    } else if (bannerInt == 2) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                        val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img2", finalFile!!.name, reqFile)
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                        Handler().postDelayed(Runnable {
                            updateProfile(
                                    username,
                                    profile_pic)
                        }, 1000)
                    } else if (bannerInt == 3) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                        val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img3", finalFile!!.name, reqFile)
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                        Handler().postDelayed(Runnable {
                            updateProfile(
                                    username,
                                    profile_pic)
                        }, 1000)
                    } else if (bannerInt == 4) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                        val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img4", finalFile!!.name, reqFile)
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                        Handler().postDelayed(Runnable {
                            updateProfile(
                                    username,
                                    profile_pic)
                        }, 1000)
                    } else if (bannerInt == 5) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                        val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("profile_pic", finalFile!!.name, reqFile)
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                        Handler().postDelayed(Runnable {
                            updateProfile(
                                    username,
                                    profile_pic)
                        }, 1000)
                    }
                } else {
                    if (activity != null)
                        Toast.makeText(activity!!, "Please change Image first", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == GALLERY_PERMISSION_CODE && resultCode == Activity.RESULT_OK) {
            var bm: Bitmap? = null
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data.data)
                    bm.compress(Bitmap.CompressFormat.JPEG, 50, ByteArrayOutputStream())
//                    profile_image.setImageBitmap(bm)

                    if (bannerInt == 1)
                        imageBanner1.setImageBitmap(bm)
                    if (bannerInt == 2)
                        imageBanner2.setImageBitmap(bm)
                    if (bannerInt == 3)
                        imageBanner3.setImageBitmap(bm)
                    if (bannerInt == 4)
                        imageBanner4.setImageBitmap(bm)
                    if (bannerInt == 5)
                        profile_image.setImageBitmap(bm)
                    val uri = data.data

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    val tempUri = getImageUri(activity!!, bm)

                    Log.e("File", "file-" + getRealPathFromURI(uri))
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    finalFile = File(getRealPathFromURI(uri))
                    if (finalFile != null) {
                        if (bannerInt == 1) {
                            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile!!)
                            val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img1", finalFile!!.name, reqFile)
                            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                            Handler().postDelayed(Runnable {
                                updateProfile(
                                        username,
                                        profile_pic)
                            }, 1000)
                        } else if (bannerInt == 2) {
                            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                            val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img2", finalFile!!.name, reqFile)
                            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                            Handler().postDelayed(Runnable {
                                updateProfile(
                                        username,
                                        profile_pic)
                            }, 1000)
                        } else if (bannerInt == 3) {
                            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                            val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img3", finalFile!!.name, reqFile)
                            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                            Handler().postDelayed(Runnable {
                                updateProfile(
                                        username,
                                        profile_pic)
                            }, 1000)
                        } else if (bannerInt == 4) {
                            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                            val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("banner_img4", finalFile!!.name, reqFile)
                            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                            Handler().postDelayed(Runnable {
                                updateProfile(
                                        username,
                                        profile_pic)
                            }, 1000)
                        } else if (bannerInt == 5) {
                            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile)
                            val profile_pic: MultipartBody.Part = MultipartBody.Part.createFormData("profile_pic", finalFile!!.name, reqFile)
                            val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.email).replace("\"", ""))
                            Handler().postDelayed(Runnable {
                                updateProfile(
                                        username,
                                        profile_pic)
                            }, 1000)
                        }
                    } else {
                        if (activity != null)
                            Toast.makeText(activity!!, "Please change Image first", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    private fun getRealPathFromURI(contentURI: Uri): String {
        val cursor = activity!!.contentResolver.query(contentURI, null, null, null, null)
        cursor.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }


    fun getJobType() {
        val progressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.user_amount(sharedprferencesDetails!!.getString(SharedprferencesDetails.username))
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "getJobType-${t!!.message}")
                progressDialog.dismiss()
                if (!call!!.isCanceled) {

                    when (t) {
                        is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                progressDialog.dismiss()
                if (activity != null) {
//                    Log.e("response_onResponse", "getJobType-${t!!.body()!!}")
                    if (t!!.code() == 200 || t.code() == 201 || t.code() == 202) {
                        val intStatus: Int = t.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            sharedprferencesDetails!!.putString(SharedprferencesDetails.wallet_amount, t.body()!!.get("message").asString)
                            textwallet.text = "$ ${sharedprferencesDetails!!.getString(SharedprferencesDetails.wallet_amount)}"
                        } else {
                            Toast.makeText(activity, t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }
}
