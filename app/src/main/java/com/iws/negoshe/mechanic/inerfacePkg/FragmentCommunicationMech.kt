package com.iws.negoshe.mechanic.inerfacePkg

import android.support.v4.app.Fragment

interface FragmentCommunicationMech {
    fun changeFragment(fragment: Fragment, title: String)
}