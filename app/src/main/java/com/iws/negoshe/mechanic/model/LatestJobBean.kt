package com.iws.negoshe.mechanic.model

import android.os.Parcel
import android.os.Parcelable

class LatestJobBean() : Parcelable {
    var job_count: Int? = null
    var profile_pic: String? = null
    var first_name: String? = null
    var job_id: Int? = null
    var work_hour: Int? = null
    var amount: Int? = null
    var user_status: Boolean? = null
    var payment_status: Boolean? = null
    var shop_status: Boolean? = null
    var mobile: String? = null
    var email: String? = null
    var job_title: String? = null
    var job_img: String? = null

    constructor(parcel: Parcel) : this() {
        job_count = parcel.readValue(Int::class.java.classLoader) as? Int
        profile_pic = parcel.readString()
        first_name = parcel.readString()
        job_id = parcel.readValue(Int::class.java.classLoader) as? Int
        work_hour = parcel.readValue(Int::class.java.classLoader) as? Int
        amount = parcel.readValue(Int::class.java.classLoader) as? Int
        user_status = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        payment_status = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        shop_status = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
        mobile = parcel.readString()
        email = parcel.readString()
        job_title = parcel.readString()
        job_img = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(job_count)
        parcel.writeString(profile_pic)
        parcel.writeString(first_name)
        parcel.writeValue(job_id)
        parcel.writeValue(work_hour)
        parcel.writeValue(amount)
        parcel.writeValue(user_status)
        parcel.writeValue(payment_status)
        parcel.writeValue(shop_status)
        parcel.writeString(mobile)
        parcel.writeString(email)
        parcel.writeString(job_title)
        parcel.writeString(job_img)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LatestJobBean> {
        override fun createFromParcel(parcel: Parcel): LatestJobBean {
            return LatestJobBean(parcel)
        }

        override fun newArray(size: Int): Array<LatestJobBean?> {
            return arrayOfNulls(size)
        }
    }


}