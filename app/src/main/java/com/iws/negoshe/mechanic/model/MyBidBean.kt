package com.iws.negoshe.mechanic.model

import android.os.Parcel
import android.os.Parcelable

class MyBidBean() : Parcelable {
    var profile_pic: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var bid_amount: Int? = null
    var bid_id: Int? = null
    var work_hours: Int? = null
    var bid_added: String? = null
    var address: String? = null
    var bid_image: String? = null
    var job_title: String? = null

    constructor(parcel: Parcel) : this() {
        profile_pic = parcel.readString()
        first_name = parcel.readString()
        last_name = parcel.readString()
        bid_amount = parcel.readValue(Int::class.java.classLoader) as? Int
        bid_id = parcel.readValue(Int::class.java.classLoader) as? Int
        work_hours = parcel.readValue(Int::class.java.classLoader) as? Int
        bid_added = parcel.readString()
        address = parcel.readString()
        bid_image = parcel.readString()
        job_title = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(profile_pic)
        parcel.writeString(first_name)
        parcel.writeString(last_name)
        parcel.writeValue(bid_amount)
        parcel.writeValue(bid_id)
        parcel.writeValue(work_hours)
        parcel.writeString(bid_added)
        parcel.writeString(address)
        parcel.writeString(bid_image)
        parcel.writeString(job_title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyBidBean> {
        override fun createFromParcel(parcel: Parcel): MyBidBean {
            return MyBidBean(parcel)
        }

        override fun newArray(size: Int): Array<MyBidBean?> {
            return arrayOfNulls(size)
        }
    }
}