package com.iws.negoshe.user.DialogPkg

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.view.Window
import com.iws.negoshe.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_feedback.*


class DialogFeedback(context: Context, i: Int) : Dialog(context) {
    private var mcontext: Context? = context
    private var rating: Int? = 0
    private var i: Int? = i
    private var feedbackComment: String? = ""
    private var name: String? = ""
    private var jobNature: String? = ""
    private var mechanic_profile: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_feedback)
        dialogDismiss.setOnClickListener {
            dismiss()
        }
        textName!!.typeface = Typeface.createFromAsset(mcontext!!.assets, "fonts/Montserrat-Medium.otf")
        textJobNature!!.typeface = Typeface.createFromAsset(mcontext!!.assets, "fonts/Montserrat-Light.otf")
        textFeedback!!.typeface = Typeface.createFromAsset(mcontext!!.assets, "fonts/Montserrat-Light.otf")

        textJobNature.text = jobNature
        textName.text = name
        textFeedback.text = feedbackComment
        ratingBar.progress = rating!!
        if (feedbackComment.equals("")) {
            textFeedback.visibility = View.GONE
        } else {
            textFeedback.visibility = View.VISIBLE
        }
        if (i == 0)
            Picasso.with(mcontext).load(mechanic_profile).error(R.drawable.ic_mechanic).placeholder(R.drawable.ic_mechanic)
                    .into(imageProfile)
        else
            Picasso.with(mcontext).load(mechanic_profile).error(R.drawable.ic_user).placeholder(R.drawable.ic_user)
                    .into(imageProfile)
    }

    fun setRating(job_rating: Int) {
        this.rating = job_rating
    }

    fun setFeedbackComment(feedbackComment: String) {
        this.feedbackComment = feedbackComment
    }

    fun setName(name: String) {
        this.name = name
    }

    fun setJobNature(job_nature: String?) {
        this.jobNature = job_nature
    }

    fun setImage(mechanic_profile: String?) {
        this.mechanic_profile = mechanic_profile

    }
}