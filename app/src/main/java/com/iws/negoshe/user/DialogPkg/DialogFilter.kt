package com.iws.negoshe.user.DialogPkg

import android.app.Dialog
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import com.iws.negoshe.R
import com.iws.negoshe.user.fragment.JobBidFragment
import kotlinx.android.synthetic.main.dialog_filter.*


class DialogFilter(context: Context, jobBidFragment: JobBidFragment) : Dialog(context) {
    private var mcontext: Context = context
    private var mjobBidFragment: JobBidFragment = jobBidFragment
    private var seekbarPriceMax: Int = 0
    private var seekbarDistanceMax: Int = 0
    private var distanceProgress: Int = 0
    private var amountProgress: Int = 0
    private var username: String? = null
    private var userflag: Int? = null
    private var userRating: Int? = 0
    private var list: ArrayList<String>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_filter)

        textFilter!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Medium.otf")
        textDistance!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textDistanceAsc!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Medium.otf")
        textAmount!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textPrice!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Medium.otf")
        textRating!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        buttonApply!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Bold.otf")

        list = ArrayList()
        list!!.add("0+")
        list!!.add("1+")
        list!!.add("2+")
        list!!.add("3+")
        list!!.add("4+")
        list!!.add("5")

        val arrayAdapter: ArrayAdapter<String> = ArrayAdapter(mcontext, android.R.layout.simple_spinner_dropdown_item, list)
        spinnerCount.adapter = arrayAdapter
        spinnerCount.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userRating = position
            }
        }
        spinnerCount.setSelection(userRating!!)

        dialogDismiss.setOnClickListener {
            dismiss()
        }

        buttonApply.setOnClickListener {
            mjobBidFragment.filterFunction(username, userflag, distanceProgress, amountProgress,userRating)
            dismiss()
        }

        seekbarDistance.max = seekbarDistanceMax
        seekbarDistance.progress = distanceProgress
        if (seekbarDistance.max != 0) {
            Handler().postDelayed(Runnable {
                val value1 = distanceProgress * (seekbarDistance!!.width - 6 * seekbarDistance.thumbOffset) / seekbarDistance.max
                textDistanceAsc.text = "$distanceProgress km"
                textDistanceAsc.x = (seekbarDistance.x + value1 + seekbarDistance.thumbOffset / 2)
            }, 500)
        }

        seekbarDistance.progressDrawable.setColorFilter(ContextCompat.getColor(mcontext, R.color.colorPrimaryUser), PorterDuff.Mode.MULTIPLY)
        seekbarDistance.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = progress * (seekBar!!.width - 6 * seekBar.thumbOffset) / seekBar.max
                textDistanceAsc.text = "$progress km"
                textDistanceAsc.x = (seekBar.x + value + seekBar.thumbOffset / 2)
                distanceProgress = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                textDistanceAsc.text = "$distanceProgress km"
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                textDistanceAsc.text = "$distanceProgress km"
            }
        })

        seekbarPrice.max = seekbarPriceMax
        seekbarPrice.progress = amountProgress
        if (seekbarPrice.max != 0) {
            Handler().postDelayed(Runnable {
                val value = amountProgress * (seekbarPrice!!.width - 7 * seekbarPrice.thumbOffset) / seekbarPrice.max
                textPrice.text = "$ ${amountProgress}"
                textPrice.x = (seekbarPrice.x + value + seekbarPrice.thumbOffset / 2)
            }, 500)
        }

        seekbarPrice.progressDrawable.setColorFilter(ContextCompat.getColor(mcontext, R.color.colorPrimaryUser), PorterDuff.Mode.MULTIPLY)
        seekbarPrice.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = progress * (seekBar!!.width - 7 * seekBar.thumbOffset) / seekBar.max
                textPrice.text = "$ ${progress}"
                textPrice.x = (seekBar.x + value + seekBar.thumbOffset / 2)
                amountProgress = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                textPrice.text = "$ $amountProgress"
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                textPrice.text = "$ $amountProgress"
            }
        })
    }

    fun seekbarPriceMaxMin(max: Int) {
        seekbarPriceMax = max
    }

    fun seekbarDistanceMaxfun(int: Double) {
        seekbarDistanceMax = Math.ceil(int).toInt()
    }

    fun setUsernameUserflag(username: String, userflag: Int) {
        this.username = username
        this.userflag = userflag
    }

    fun setDistanceAmountProgress(distance: Int, amount: Int) {
        this.distanceProgress = distance
        this.amountProgress = amount
    }

    fun setRating(userRating: Int) {
       this.userRating = userRating
    }

}