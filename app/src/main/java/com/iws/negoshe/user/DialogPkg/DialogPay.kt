package com.iws.negoshe.user.DialogPkg

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.model.request.UserRunningJobModel.Message
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.user.fragment.DashboardRunningFragment
import kotlinx.android.synthetic.main.dialog_pay.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException


class DialogPay(context: Context, message: Message, dashboardRunningFragment: DashboardRunningFragment) : Dialog(context) {
    private var mMessage: Message = message
    private var mcontext: Context = context
    private var mdashboardRunningFragment: DashboardRunningFragment = dashboardRunningFragment
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_pay)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(mcontext)

        textConfirm.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Medium.otf")
        textAmount.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        buttonApply!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Bold.otf")

        textAmount.text = Html.fromHtml("Sure to pay amount <b>$ ${mMessage.bidAmount}</b> to <b>${mMessage.firstName + " " + mMessage.lastName}</b>?")
        dialogDismiss.setOnClickListener {
            dismiss()
        }

        buttonApply.setOnClickListener {
            onClickComplete(sharedprferencesDetails!!.getString(SharedprferencesDetails.username),
                    mMessage.jobId)
        }


    }

    private fun onClickComplete(username: String, job_id: Int) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("job_id", job_id)
        bid_payment(jsonObject)

    }

    fun bid_payment(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(mcontext)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(mcontext).create(ApiClient.ApiInterface::class.java)
        val call = apiService.bid_payment(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(mcontext, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(mcontext, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(mcontext, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            Toast.makeText(mcontext, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            sharedprferencesDetails!!.putString(SharedprferencesDetails.wallet_amount, response.body()!!.get("user_amount").asString)
                            mdashboardRunningFragment.onResume()
                            dismiss()
                        } else {

                            if (response.body()!!.has("message"))
                                Toast.makeText(mcontext, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            else if (response.body()!!.has("data"))
                                Toast.makeText(mcontext, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                            dismiss()
                        }
                    }
                } else {
                    Toast.makeText(mcontext, mcontext.getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}