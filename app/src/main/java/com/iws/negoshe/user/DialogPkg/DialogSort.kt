package com.iws.negoshe.user.DialogPkg

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Window
import com.iws.negoshe.R
import com.iws.negoshe.user.fragment.JobBidFragment
import kotlinx.android.synthetic.main.dialog_sort.*


class DialogSort(context: Context, jobBidFragment: JobBidFragment) : Dialog(context) {
    private var mcontext: Context = context
    private var mjobBidFragment: JobBidFragment = jobBidFragment
    private var sortFun: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_sort)
        dialogDismiss.setOnClickListener {
            dismiss()
        }
        textSort!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Medium.otf")
        textDistanceAsc!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textDistanceDesc!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textRating!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textCostL2H!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")
        textCostH2L!!.typeface = Typeface.createFromAsset(mcontext.assets, "fonts/Montserrat-Regular.otf")

        textDistanceAsc.setOnClickListener {
            textDistanceAsc.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorAccentUser))
            textDistanceDesc.setBackgroundResource(0)
            textRating.setBackgroundResource(0)
            textCostL2H.setBackgroundResource(0)
            textCostH2L.setBackgroundResource(0)
            sortFun = 0
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }
        textDistanceDesc.setOnClickListener {
            textDistanceDesc.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorAccentUser))
            textDistanceAsc.setBackgroundResource(0)
            textRating.setBackgroundResource(0)
            textCostL2H.setBackgroundResource(0)
            textCostH2L.setBackgroundResource(0)
            sortFun = 1
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }
        textRating.setOnClickListener {
            textRating.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorAccentUser))
            textDistanceAsc.setBackgroundResource(0)
            textDistanceDesc.setBackgroundResource(0)
            textCostL2H.setBackgroundResource(0)
            textCostH2L.setBackgroundResource(0)
            sortFun = 2
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }
        textCostL2H.setOnClickListener {
            textCostL2H.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorAccentUser))
            textDistanceAsc.setBackgroundResource(0)
            textDistanceDesc.setBackgroundResource(0)
            textRating.setBackgroundResource(0)
            textCostH2L.setBackgroundResource(0)
            sortFun = 3
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }
        textCostH2L.setOnClickListener {
            textCostH2L.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorAccentUser))
            textDistanceAsc.setBackgroundResource(0)
            textDistanceDesc.setBackgroundResource(0)
            textRating.setBackgroundResource(0)
            textCostL2H.setBackgroundResource(0)
            sortFun = 4
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }
        /*buttonApply.setOnClickListener {
            mjobBidFragment.sortFunction(sortFun)
            dismiss()
        }*/
    }
}