package com.iws.negoshe.user.fragment


import android.app.ProgressDialog
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.utils.AsteriskPasswordTransformationMethod
import kotlinx.android.synthetic.main.fragment_change_password_user.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChangePasswordMechFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ChangePasswordUserFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_password_user, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChangePasswordMechFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                ChangePasswordUserFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)

        inputOldpass.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        inputNewPassword.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        inputConfirmPassword.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        buttonSubmit.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")

        buttonSubmit.setOnClickListener {
            if (!inputOldpass.text.toString().equals(sharedprferencesDetails!!.getString(SharedprferencesDetails.password))) {
                Toast.makeText(activity!!, "Old password is not correct", Toast.LENGTH_SHORT).show()
            } else if (inputNewPassword.text.toString().length < 8)
                Toast.makeText(activity!!, "Enter min 8 digit new password.", Toast.LENGTH_SHORT).show()
            else if (inputConfirmPassword.text.toString().length < 8)
                Toast.makeText(activity!!, "Enter min 8 digit confirm password.", Toast.LENGTH_SHORT).show()
            else if (!inputNewPassword.text.toString().equals(inputConfirmPassword.text.toString(), true))
                Toast.makeText(activity!!, "Password and Confirm password does not match.", Toast.LENGTH_SHORT).show()
            else
                onClickSignup(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), inputOldpass.text.toString(),
                        inputNewPassword.text.toString(), inputConfirmPassword.text.toString())
        }
        inputOldpass!!.transformationMethod = AsteriskPasswordTransformationMethod()
        inputNewPassword!!.transformationMethod = AsteriskPasswordTransformationMethod()
        inputConfirmPassword!!.transformationMethod = AsteriskPasswordTransformationMethod()
    }


    private fun onClickSignup(user_name: String, old_password: String, new_password: String, confirm_password: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user_name", user_name)
        jsonObject.addProperty("old_password", old_password)
        jsonObject.addProperty("new_password", new_password)
        jsonObject.addProperty("confirm_password", confirm_password)
        Log.e("request_login", "login-$jsonObject")

        changepass(jsonObject)
    }

    private fun changepass(change: JsonObject) {

        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.change_nego_pwd(change)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "signup-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "signup-${t!!.body().toString()}")
                progressDialog.dismiss()
                if (t.code() == 200||t.code() == 201||t.code() == 202) {
                    if (t.body()!!.isJsonObject) {
                        val intStatus: String = t.body()!!.get("status").asString
                        if (intStatus.equals("1")) {
                            Toast.makeText(activity!!, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
//                        startActivity(Intent(activity!!, LoginActivity::class.java))
//                        finish()
                            sharedprferencesDetails!!.putString(SharedprferencesDetails.password, inputNewPassword.text.toString())
                            activity!!.onBackPressed()
                        } else {
                            when {
                                t.body()!!.has("message") -> Toast.makeText(activity!!, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("data") -> Toast.makeText(activity!!, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("error") -> Toast.makeText(activity!!, "" + t.body()!!.get("error").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(activity!!, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
