package com.iws.negoshe.user.fragment


import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.iws.negoshe.R
import kotlinx.android.synthetic.main.fragment_dashboard.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DashboardFragment.
         */

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DashboardFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addFragment(DashboardRunningFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))

        textRunning.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textPending.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textCompleted.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")

        constraintRunningJob.setOnClickListener {
            addFragment(DashboardRunningFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewPending.visibility = View.GONE
            viewRunning.visibility = View.VISIBLE
            viewCompleted.visibility = View.GONE
            textRunning.setTextColor(ContextCompat.getColor(activity!!, R.color.colorEdtHintUser))
            textPending.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
//            imgRunning.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.running_blue))
//            imgPending.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.pending_grey))
        }
        constraintPendingJob.setOnClickListener {
            addFragment(DashboardPendingFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewPending.visibility = View.VISIBLE
            viewRunning.visibility = View.GONE
            viewCompleted.visibility = View.GONE
            textRunning.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
            textPending.setTextColor(ContextCompat.getColor(activity!!, R.color.colorEdtHintUser))
//            imgRunning.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.running_grey))
//            imgPending.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.pending_blue))
        }
        constraintCompleted.setOnClickListener {
            addFragment(DashboardCompletedFragment.newInstance("", ""), getString(R.string.title_toolbar_dashboard))
            viewRunning.visibility = View.GONE
            viewCompleted.visibility = View.VISIBLE
            viewPending.visibility = View.GONE
            textRunning.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
            textPending.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
            textCompleted.setTextColor(ContextCompat.getColor(activity!!, R.color.colorEdtHintUser))
//            imageOngoing.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ongoing_job_selected))
//            imageLatest.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.latest_job_unselected))
        }
    }

    //Add Dynamic fragment to framelayout
    private fun addFragment(fragment: Fragment, title: String) {
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.framelayoutChild, fragment, title)
        fragmentTransaction.commitAllowingStateLoss()
    }
}
