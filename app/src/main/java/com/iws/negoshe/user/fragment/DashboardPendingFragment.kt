package com.iws.negoshe.user.fragment


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.BidDetailActivity
import com.iws.negoshe.user.inerfacePkg.FragmentCommunication
import com.iws.negoshe.user.model.PendingJobBean
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_dashboard_pending.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardPendingFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardPendingFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var latestJobAdapter: LatestJobAdapter? = null
    private var fragmentCommunication: FragmentCommunication? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var listLatestJobBean: ArrayList<PendingJobBean>? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentCommunication = context as FragmentCommunication
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard_pending, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DashboardPendingFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                DashboardPendingFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        listLatestJobBean = ArrayList()
        latestJobAdapter = LatestJobAdapter()
        recyclerViewPending.layoutManager = LinearLayoutManager(activity)
//        recyclerViewLatest.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recyclerViewPending.adapter = latestJobAdapter
    }


    inner class LatestJobAdapter : RecyclerView.Adapter<LatestJobAdapter.MyViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard_pending, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {
//            return if (listDistributorListBean!!.size > 0)
//                listDistributorListBean!![0].data.size
//            else
            return listLatestJobBean!!.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.textTitle!!.text = listLatestJobBean!![position].job_title
            holder.textAmount!!.visibility = View.GONE
            holder.rating!!.visibility = View.GONE
//            holder.textName!!.text = listLatestJobBean!![position].first_name
//            holder.textEmail!!.text = listLatestJobBean!![position].email
//            holder.textNumber!!.text = listLatestJobBean!![position].mobile
            holder.bind(position)
            Picasso.with(activity!!).load(listLatestJobBean!![position].profile_pic)
                    .placeholder(R.drawable.ic_user).into(holder.profile_image)
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(position: Int) {
                textBidDetail!!.setOnClickListener {
                    fragmentCommunication!!.changeFragment(JobBidFragment.newInstance(listLatestJobBean!![position].job_id!!, ""), listLatestJobBean!![position].job_title!!)
                }
                textDetail!!.setOnClickListener {
                    val intent = Intent(activity!!, BidDetailActivity::class.java)
//                    val bundle = Bundle()
//                    bundle.putParcelable("bundle", listLatestJobBean!![0].message[position])
                    intent.putExtra("title", listLatestJobBean!![position].job_title)
                    intent.putExtra("image", listLatestJobBean!![position].job_image)
                    startActivity(intent)
                }
                textTitle!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textAssign!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
                textDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textBidDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                if (listLatestJobBean!![position].job_image.equals("media/Pictures/job.png"))
                    textDetail!!.visibility = View.INVISIBLE
                else
                    textDetail!!.visibility = View.VISIBLE
                textBidDetail!!.visibility = View.VISIBLE
            }

            var profile_image: CircleImageView? = itemView.findViewById(R.id.profile_image)
            var textTitle: TextView? = itemView.findViewById(R.id.textTitle)
            var textBidDetail: TextView? = itemView.findViewById(R.id.textBidDetail)
            var textAmount: TextView? = itemView.findViewById(R.id.textAmount)
            var textAssign: TextView? = itemView.findViewById(R.id.textAssign)
            var textDetail: TextView? = itemView.findViewById(R.id.textDetail)
            var rating: RatingBar? = itemView.findViewById(R.id.rating)
        }
    }


    override fun onResume() {
        super.onResume()
        onClickgetLatest(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner))
    }

    private fun onClickgetLatest(username: String, userFlag: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
//        jsonObject.addProperty("user_flag", userFlag)
        pendingJob(jsonObject)

    }

    fun pendingJob(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.search_job_user(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> if (activity != null) Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        listLatestJobBean!!.clear()
                        if (intStatus == 1) {
                            val arrayData: JsonArray = response.body()!!.get("job_title").asJsonArray
                            for (i in 0 until arrayData.size()) {
                                val objectData: JsonObject = arrayData.get(i).asJsonObject
                                val latestJobBean = PendingJobBean()
                                latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                }
                                latestJobBean.job_image = when (objectData.get("job_image").isJsonNull) {true -> ""
                                    false -> objectData.get("job_image").asString
                                }
                                latestJobBean.is_active = when (objectData.get("is_active").isJsonNull) {true -> ""
                                    false -> objectData.get("is_active").asString
                                }
                                latestJobBean.job_id = when (objectData.get("job_id").isJsonNull) {true -> 0
                                    false -> objectData.get("job_id").asInt
                                }
                                latestJobBean.job_title = when (objectData.get("job_title").isJsonNull) {true -> ""
                                    false -> objectData.get("job_title").asString
                                }
                                listLatestJobBean!!.add(latestJobBean)
                            }
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                        latestJobAdapter!!.notifyDataSetChanged()
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
