package com.iws.negoshe.user.fragment


import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.app.activity.BidDetailActivity
import com.iws.negoshe.user.activity.ProfileActivity
import com.iws.negoshe.user.model.BidListBean
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_job_bid.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [JobBidFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class JobBidFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    override fun onMarkerClick(p0: Marker?): Boolean {
        try {
            if (activity != null) {
                val intent = Intent(activity!!, ProfileActivity::class.java)
                intent.putExtra("username", listLatestFilterBean!![mHashMapId[p0]!!].username)
                startActivity(intent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return true
    }

    private val mHashMapId = HashMap<Marker, Int>()
    private var map: GoogleMap? = null
    private var listMarker: ArrayList<Marker>? = null
    override fun onMapReady(p0: GoogleMap?) {
        map = p0
        map!!.setOnMarkerClickListener(this@JobBidFragment)
        listMarker = ArrayList()
        for (i in 0 until listLatestFilterBean!!.size) {
            val marker = p0!!.addMarker(MarkerOptions()
                    .position(LatLng(listLatestFilterBean!![i].mechanic_lat!!, listLatestFilterBean!![i].mechanic_lng!!))
                    .title(listLatestFilterBean!![i].first_name + " " + listLatestFilterBean!![i].last_name))
            listMarker!!.add(marker)

            p0.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(LatLng(listLatestFilterBean!![i].mechanic_lat!!, listLatestFilterBean!![i].mechanic_lng!!), 12f))

            mHashMapId[marker] = i
//            p0.setOnMarkerClickListener(this);
//            marker.showInfoWindow();
        }
        /*p0!!.setOnInfoWindowClickListener {
            val intent = Intent(activity!!, ProfileActivity::class.java)
            intent.putExtra("username", listLatestFilterBean!![mHashMapId[p0]!!].username)
            startActivity(intent)
        }*/

    }

    internal var mapFragment: SupportMapFragment? = null
    private var param1: Int? = null
    private var param2: String? = null
    private var latestJobAdapter: LatestJobAdapter? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var listLatestJobBean: ArrayList<BidListBean>? = null
    private var listLatestFilterBean: ArrayList<BidListBean>? = null
    private var sortValue: Int = 0
    private var priceMaxValue: Int = 0
    private var distanceProgress: Int = 0
    private var amountProgress: Int = 0
    private var userRating: Int = 0

    fun sortFunction(int: Int) {
        sortValue = int
        when (sortValue) {
            0 -> Collections.sort(listLatestFilterBean, CustomComparator())
            1 -> Collections.sort(listLatestFilterBean, Collections.reverseOrder(CustomComparator()))
            2 -> Collections.sort(listLatestFilterBean, Collections.reverseOrder(CustomComparator()))
            3 -> Collections.sort(listLatestFilterBean, CustomComparator())
            4 -> Collections.sort(listLatestFilterBean, Collections.reverseOrder(CustomComparator()))
        }
        latestJobAdapter!!.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getInt(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)

        listLatestJobBean = ArrayList()
        listLatestFilterBean = ArrayList()

        textSort.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
        textFilter.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")

        mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        val ft = childFragmentManager.beginTransaction()
        ft.hide(mapFragment).commit()

        latestJobAdapter = LatestJobAdapter()
        recyclerViewBid.layoutManager = LinearLayoutManager(activity)
        recyclerViewBid.adapter = latestJobAdapter

        linearSort.setOnClickListener {
            if (recyclerViewBid.visibility == View.VISIBLE) {
                val dialogsort: com.iws.negoshe.user.DialogPkg.DialogSort = com.iws.negoshe.user.DialogPkg.DialogSort(activity!!, this@JobBidFragment)
                dialogsort.setCancelable(false)
                dialogsort.show()
            } else {
                Toast.makeText(activity!!, "Sorting Only Works On List. Switch To List.", Toast.LENGTH_SHORT).show()
            }
        }
        linearFilter.setOnClickListener {
            if (listLatestJobBean!!.size > 0) {
                val maxPrice: BidListBean = Collections.max(listLatestJobBean, BidAmount())
                priceMaxValue = maxPrice.quote_amount!!

                val maxDistance: BidListBean = Collections.max(listLatestJobBean, BidDistance())


                val dialogfilter = com.iws.negoshe.user.DialogPkg.DialogFilter(activity!!, this@JobBidFragment)
                dialogfilter.setCancelable(false)
                dialogfilter.seekbarPriceMaxMin(priceMaxValue)
                dialogfilter.setDistanceAmountProgress(distanceProgress, amountProgress)
                dialogfilter.setRating(userRating)
                dialogfilter.seekbarDistanceMaxfun(maxDistance.distance!!)
                dialogfilter.setUsernameUserflag(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), param1!!)
                dialogfilter.show()
            } else {
                Toast.makeText(activity!!, "No Bid Made On This Job For Filter.", Toast.LENGTH_SHORT).show()
            }
        }
        onClickgetLatest(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), param1!!)

        imageMap.setOnClickListener {
            recyclerViewBid.visibility = View.GONE
            val ft1 = childFragmentManager.beginTransaction()
            ft1.show(mapFragment).commit()

            imageMap.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_map))
            imagelist.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_menu_grey))
        }
        imagelist.setOnClickListener {
            recyclerViewBid.visibility = View.VISIBLE
            val ft1 = childFragmentManager.beginTransaction()
            ft1.hide(mapFragment).commit()
            imageMap.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_map_grey))
            imagelist.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_menu))
            updateMap()
        }
    }

    private fun updateMap() {
        if (listMarker != null) {
            for (i in 0 until listMarker!!.size) {
                listMarker!![i].remove()
            }
        }
        mHashMapId.clear()
//        map!!.setOnMarkerClickListener(this@JobBidFragment)
        for (i in 0 until listLatestFilterBean!!.size) {

            val marker = map!!.addMarker(MarkerOptions()
                    .position(LatLng(listLatestFilterBean!![i].mechanic_lat!!, listLatestFilterBean!![i].mechanic_lng!!))
                    .title(listLatestFilterBean!![i].first_name + " " + listLatestFilterBean!![i].last_name))
            listMarker!!.add(marker)
            map!!.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(LatLng(listLatestFilterBean!![i].mechanic_lat!!, listLatestFilterBean!![i].mechanic_lng!!), 12f))
            mHashMapId[marker] = i
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_job_bid, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment JobBidFragment.
         */
        @JvmStatic
        fun newInstance(param1: Int, param2: String) =
                JobBidFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }


    private fun onClickgetLatest(username: String, userFlag: Int) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("job_id", userFlag)
        jobBid(jsonObject)

    }

    private fun jobBid(login: JsonObject) {
        val progressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.short_bid(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        listLatestFilterBean!!.clear()
                        listLatestJobBean!!.clear()
                        if (intStatus == 1) {
                            val arrayData: JsonArray = response.body()!!.get("message").asJsonArray
                            if (arrayData.size() > 0) {
                                for (i in 0 until arrayData.size()) {
                                    val objectData: JsonObject = arrayData.get(i).asJsonObject
                                    val latestJobBean = BidListBean()
                                    latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                        false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                    }
                                    latestJobBean.username = when (objectData.get("username").isJsonNull) {true -> ""
                                        false -> objectData.get("username").asString
                                    }
                                    latestJobBean.distance = when (objectData.get("distance").isJsonNull) {true -> 0.0
                                        false -> objectData.get("distance").asDouble
                                    }
                                    latestJobBean.first_name = when (objectData.get("first_name").isJsonNull) {true -> ""
                                        false -> objectData.get("first_name").asString
                                    }
                                    latestJobBean.last_name = when (objectData.get("last_name").isJsonNull) {true -> ""
                                        false -> objectData.get("last_name").asString
                                    }
                                    latestJobBean.bid_image = when (objectData.get("bid_image").isJsonNull) {true -> ""
                                        false -> objectData.get("bid_image").asString
                                    }
                                    latestJobBean.working_hours = when (objectData.get("working_hours").isJsonNull) {true -> 0
                                        false -> objectData.get("working_hours").asInt
                                    }
                                    latestJobBean.quote_amount = when (objectData.get("quote_amount").isJsonNull) {true -> 0
                                        false -> objectData.get("quote_amount").asInt
                                    }
                                    latestJobBean.bid_id = when (objectData.get("bid_id").isJsonNull) {true -> 0
                                        false -> objectData.get("bid_id").asInt
                                    }
                                    latestJobBean.mechanic_lat = when (objectData.get("mechanic_lat").isJsonNull) {true -> 0.0
                                        false -> objectData.get("mechanic_lat").asDouble
                                    }
                                    latestJobBean.mechanic_lng = when (objectData.get("mechanic_lng").isJsonNull) {true -> 0.0
                                        false -> objectData.get("mechanic_lng").asDouble
                                    }
                                    val mechanic_rating: JsonObject = objectData.getAsJsonObject("mechanic_rating")
                                    latestJobBean.mechnic_rating__avg = when (mechanic_rating.get("user_rating__avg").isJsonNull) {true -> 0.0F
                                        false -> mechanic_rating.get("user_rating__avg").asFloat
                                    }
                                    listLatestJobBean!!.add(latestJobBean)
                                    listLatestFilterBean!!.add(latestJobBean)
                                }
                            } else {
                                Toast.makeText(activity!!, "No bid made on this job yet", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                        latestJobAdapter!!.notifyDataSetChanged()
                        mapFragment!!.getMapAsync(this@JobBidFragment)
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    fun jobBidFilter(login: JsonObject, distanceProgress: Int, amountProgress: Int, userRating: Int) {
        this.distanceProgress = distanceProgress
        this.amountProgress = amountProgress
        this.userRating = userRating

        val progressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.short_bid(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}")
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        listLatestFilterBean!!.clear()
                        listLatestJobBean!!.clear()
                        if (intStatus == 1) {
                            val arrayData: JsonArray = response.body()!!.get("message").asJsonArray
                            for (i in 0 until arrayData.size()) {
                                val objectData: JsonObject = arrayData.get(i).asJsonObject
                                val latestJobBean = BidListBean()
                                latestJobBean.profile_pic = when (objectData.get("profile_pic").isJsonNull) {true -> ""
                                    false -> ApiClient.BASE_URL_Image + objectData.get("profile_pic").asString
                                }
                                latestJobBean.username = when (objectData.get("username").isJsonNull) {true -> ""
                                    false -> objectData.get("username").asString
                                }
                                latestJobBean.distance = when (objectData.get("distance").isJsonNull) {true -> 0.0
                                    false -> objectData.get("distance").asDouble
                                }
                                latestJobBean.first_name = when (objectData.get("first_name").isJsonNull) {true -> ""
                                    false -> objectData.get("first_name").asString
                                }
                                latestJobBean.last_name = when (objectData.get("last_name").isJsonNull) {true -> ""
                                    false -> objectData.get("last_name").asString
                                }
                                latestJobBean.bid_image = when (objectData.get("bid_image").isJsonNull) {true -> ""
                                    false -> objectData.get("bid_image").asString
                                }
                                latestJobBean.working_hours = when (objectData.get("working_hours").isJsonNull) {true -> 0
                                    false -> objectData.get("working_hours").asInt
                                }
                                latestJobBean.quote_amount = when (objectData.get("quote_amount").isJsonNull) {true -> 0
                                    false -> objectData.get("quote_amount").asInt
                                }
                                latestJobBean.bid_id = when (objectData.get("bid_id").isJsonNull) {true -> 0
                                    false -> objectData.get("bid_id").asInt
                                }
                                latestJobBean.mechanic_lat = when (objectData.get("mechanic_lat").isJsonNull) {true -> 0.0
                                    false -> objectData.get("mechanic_lat").asDouble
                                }
                                latestJobBean.mechanic_lng = when (objectData.get("mechanic_lng").isJsonNull) {true -> 0.0
                                    false -> objectData.get("mechanic_lng").asDouble
                                }
                                val mechanic_rating: JsonObject = objectData.getAsJsonObject("mechanic_rating")
                                latestJobBean.mechnic_rating__avg = when (mechanic_rating.get("user_rating__avg").isJsonNull) {true -> 0.0F
                                    false -> mechanic_rating.get("user_rating__avg").asFloat
                                }
                                /* if (distanceProgress != 0 && amountProgress != 0) {
                                     if (distanceProgress >= objectData.get("distance").asDouble && amountProgress >= objectData.get("quote_amount").asInt)
                                         listLatestFilterBean!!.add(latestJobBean)
                                 } else if (distanceProgress != 0 && amountProgress == 0) {
                                     if (distanceProgress >= objectData.get("distance").asDouble)
                                         listLatestFilterBean!!.add(latestJobBean)
                                 } else if (distanceProgress == 0 && amountProgress != 0) {
                                     if (amountProgress >= objectData.get("quote_amount").asDouble)
                                         listLatestFilterBean!!.add(latestJobBean)
                                 } else {
                                     listLatestFilterBean!!.add(latestJobBean)
                                 }*/
                                Log.e("rating", "rating- " + userRating + " " + latestJobBean.mechnic_rating__avg!!)
                                if (distanceProgress != 0 && amountProgress != 0 && userRating != 0) {
                                    if (distanceProgress >= objectData.get("distance").asDouble && amountProgress >= objectData.get("quote_amount").asInt
                                            && userRating <= latestJobBean.mechnic_rating__avg!!)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress != 0 && amountProgress != 0 && userRating == 0) {
                                    if (distanceProgress >= objectData.get("distance").asDouble && amountProgress >= objectData.get("quote_amount").asInt)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress != 0 && amountProgress == 0 && userRating != 0) {
                                    if (distanceProgress >= objectData.get("distance").asDouble && userRating <= latestJobBean.mechnic_rating__avg!!)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress != 0 && amountProgress == 0 && userRating == 0) {
                                    if (distanceProgress >= objectData.get("distance").asDouble)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress == 0 && amountProgress != 0 && userRating != 0) {
                                    if (amountProgress >= objectData.get("quote_amount").asInt
                                            && userRating <= latestJobBean.mechnic_rating__avg!!)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress == 0 && amountProgress != 0 && userRating == 0) {
                                    if (amountProgress >= objectData.get("quote_amount").asInt)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else if (distanceProgress == 0 && amountProgress == 0 && userRating != 0) {
                                    if (userRating <= latestJobBean.mechnic_rating__avg!!)
                                        listLatestFilterBean!!.add(latestJobBean)
                                } else {
                                    listLatestFilterBean!!.add(latestJobBean)
                                }

                                listLatestJobBean!!.add(latestJobBean)
                            }
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }
                        latestJobAdapter!!.notifyDataSetChanged()
                        updateMap()
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    inner class LatestJobAdapter : RecyclerView.Adapter<LatestJobAdapter.MyViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_bid, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {
            return listLatestFilterBean!!.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.textTitle!!.text = listLatestFilterBean!![position].first_name + " " + listLatestFilterBean!![position].last_name
            holder.textDistance!!.text = "${listLatestFilterBean!![position].distance} Km away"
            holder.textAssign!!.text = "Amount- $ ${listLatestFilterBean!![position].quote_amount}"
            holder.textWorkHour!!.text = "Duration- ${listLatestFilterBean!![position].working_hours} Hr"
            holder.rating!!.rating = listLatestFilterBean!![position].mechnic_rating__avg!!.toFloat()
            holder.bind(position)
            Picasso.with(activity!!).load(listLatestFilterBean!![position].profile_pic)
                    .placeholder(R.drawable.ic_mechanic).into(holder.profile_image)

//            Log.e("bidImage", listLatestFilterBean!![position].bid_image)

        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var profile_image: CircleImageView? = itemView.findViewById(R.id.profile_image)
            var textTitle: TextView? = itemView.findViewById(R.id.textTitle)
            var textDistance: TextView? = itemView.findViewById(R.id.textDistance)
            var textAssign: TextView? = itemView.findViewById(R.id.textAssign)
            var textWorkHour: TextView? = itemView.findViewById(R.id.textWorkHour)
            var buttonSelectBid: TextView? = itemView.findViewById(R.id.buttonSelectBid)
            var textBidDetail: TextView? = itemView.findViewById(R.id.textBidDetail)
            var rating: RatingBar? = itemView.findViewById(R.id.rating)
            fun bind(position: Int) {
                buttonSelectBid!!.setOnClickListener {
                    onClickBid(sharedprferencesDetails!!.getString(SharedprferencesDetails.username), listLatestFilterBean!![position].bid_id.toString())
                }

                textTitle!!.setOnClickListener {
                    val intent = Intent(activity!!, ProfileActivity::class.java)
                    intent.putExtra("username", listLatestFilterBean!![position].username)
                    startActivity(intent)
                }

                profile_image!!.setOnClickListener {
                    val intent = Intent(activity!!, ProfileActivity::class.java)
                    intent.putExtra("username", listLatestFilterBean!![position].username)
                    startActivity(intent)
                }

                textBidDetail!!.setOnClickListener {
                    val intent = Intent(activity!!, BidDetailActivity::class.java)
                    intent.putExtra("title", "")
                    intent.putExtra("image", listLatestFilterBean!![position].bid_image)
                    startActivity(intent)
                }

                if (listLatestJobBean!![position].bid_image.equals("media/Pictures/job.png"))
                    textBidDetail!!.visibility = View.INVISIBLE
                else
                    textBidDetail!!.visibility = View.VISIBLE

                textTitle!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textAssign!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textWorkHour!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textDistance!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                buttonSelectBid!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")
                textBidDetail!!.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-SemiBold.otf")

            }
        }
    }


    private fun onClickBid(username: String, bid_id: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("bid_id", bid_id)
        Log.e("request_login", "login-$jsonObject")

        changepass(jsonObject)
    }

    private fun changepass(change: JsonObject) {

        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.place_bid(change)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "signup-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "signup-${t!!.body().toString()}")
                progressDialog.dismiss()
                if (t.code() == 200 || t.code() == 201 || t.code() == 202) {
                    if (t.body()!!.isJsonObject) {
                        val intStatus: String = t.body()!!.get("status").asString
                        if (intStatus.equals("1")) {
                            Toast.makeText(activity!!, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
//                        startActivity(Intent(activity!!, LoginActivity::class.java))
//                        finish()
                            activity!!.onBackPressed()
                        } else {
                            when {
                                t.body()!!.has("message") -> Toast.makeText(activity!!, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("data") -> Toast.makeText(activity!!, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                                t.body()!!.has("error") -> Toast.makeText(activity!!, "" + t.body()!!.get("error").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(activity!!, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    fun filterFunction(username: String?, userFlag: Int?, distanceProgress: Int, amountProgress: Int, userRating: Int?) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("job_id", userFlag)
        jobBidFilter(jsonObject, distanceProgress, amountProgress, userRating!!)
    }

    inner class CustomComparator : Comparator<BidListBean> {
        override fun compare(o1: BidListBean, o2: BidListBean): Int {
            when (sortValue) {
                0 -> return o1.distance!!.compareTo(o2.distance!!)
                1 -> return o1.distance!!.compareTo(o2.distance!!)
                2 -> return o1.mechnic_rating__avg!!.compareTo(o2.mechnic_rating__avg!!)
                3 -> return o1.quote_amount!!.compareTo(o2.quote_amount!!)
                4 -> return o1.quote_amount!!.compareTo(o2.quote_amount!!)
                else -> return o1.bid_id!!.compareTo(o2.bid_id!!)
            }
        }
    }


    inner class BidAmount : Comparator<BidListBean> {

        override fun compare(e1: BidListBean, e2: BidListBean): Int {
            return e1.quote_amount!!.compareTo(e2.quote_amount!!)
        }
    }

    inner class BidDistance : Comparator<BidListBean> {

        override fun compare(e1: BidListBean, e2: BidListBean): Int {
            return e1.distance!!.compareTo(e2.distance!!)
        }
    }
}
