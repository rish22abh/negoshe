package com.iws.negoshe.user.fragment


import android.app.ProgressDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.iws.commontask.model.request.CommentListBean
import com.iws.commontask.model.request.PostList
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_post_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketException
import java.net.SocketTimeoutException

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PostDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PostDetailFragment : Fragment() {
    private var param1: PostList? = null
    private var param2: String? = null
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    private var loading = true
    private var pagePost: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getParcelable(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_detail, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PostDetailFragment.
         */
        @JvmStatic
        fun newInstance(param1: PostList, param2: String) =
                PostDetailFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)
        Picasso.with(activity!!).load(param1!!.post_image).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imagePostDetail)

        textTitleDetail.text = param1!!.post_title

        textDescriptionDetail.text = param1!!.post_description
        if (sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner).equals("1"))
            buttonComment.background = ContextCompat.getDrawable(activity!!, R.drawable.button_gradient_user)
        else
            buttonComment.background = ContextCompat.getDrawable(activity!!, R.drawable.button_gradient_mechanic)

        buttonComment.setOnClickListener {
            hideKeyboard()
            if (edtComment.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Enter Comment.", Toast.LENGTH_SHORT).show()
            } else {
                onClicksubmitComment(edtComment.text.toString())
            }
        }


        textTitleDetail.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
        textCommentHeading.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
        textDescriptionDetail.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
        edtComment.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
        buttonComment.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")

        scrollView.viewTreeObserver.addOnScrollChangedListener(object : ViewTreeObserver.OnScrollChangedListener {
            override fun onScrollChanged() {
                if (scrollView != null) {
                    if (scrollView.getChildAt(0).bottom <= (scrollView.height + scrollView.scrollY)) {
                        //scroll view is at bottom
                        Log.e("erere", "Last Item Wow !")
                        if (loading) {
//                            loading = false
                            pagePost += 1
                            onResume()
                            Log.e("erere", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data

                        }
                    } else {
                        //scroll view is not at bottom
                        Log.e("erere", "Last Item Not !")
                    }
                }
            }

        })
    }


    override fun onResume() {
        super.onResume()
        edtComment.setText("")
        getComment()
    }


    private fun getComment() {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.comment_list(param1!!.post_id.toString(), pagePost)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "getJobType-${t!!.message}")
                progressDialog.dismiss()
                if (!call!!.isCanceled) {
                    when (t) {
                        is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
//                Log.e("response_onResponse", "getJobType-${t!!.body()!!.toString()}")
                progressDialog.dismiss()
                if (t!!.code() == 200 || t.code() == 201 || t.code() == 202) {
                    if (!t.body()!!.equals(null)) {
                        val intStatus: Int = t.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            val jsonArray = t.body()!!.getAsJsonArray("message")
//                            linearListComment.removeAllViews()
                            if (jsonArray.size() != 5) {
                                loading = false
                            }
                            for (i in 0 until jsonArray.size()) {
                                val jsonObject = jsonArray[i].asJsonObject
                                val postList = CommentListBean()
                                postList.post_id = jsonObject.get("post_id").asInt
                                postList.post_comment = jsonObject.get("post_comment").asString
                                postList.username = jsonObject.get("username").asString
                                postList.comment_date_added = jsonObject.get("comment_date_added").asString
                                postList.first_name = jsonObject.get("first_name").asString
                                postList.last_name = jsonObject.get("last_name").asString
                                postList.profile_pic = ApiClient.BASE_URL_Image + jsonObject.get("profile_pic").asString

                                val child = layoutInflater.inflate(R.layout.item_comment, null)
                                val textComment = child.findViewById<TextView>(R.id.textComment)
                                val textUser = child.findViewById<TextView>(R.id.textUser)
                                val profile_image = child.findViewById<CircleImageView>(R.id.profile_image)
                                textComment.text = postList.post_comment
                                textUser.text = postList.first_name + " " + postList.last_name
//                                textUser.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14.0F)
                                textUser.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                                textComment.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
//                                textComment.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12.5F)
                                Picasso.with(activity!!).load(postList.profile_pic).error(R.drawable.ic_user_placeholder)
                                        .placeholder(R.drawable.ic_user_placeholder).into(profile_image)
                                linearListComment.addView(child)
                            }
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }

            }
        })
    }


    fun hideKeyboard() {

        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(textTitleDetail.windowToken, 0)

    }

    private fun onClicksubmitComment(post_comment: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", sharedprferencesDetails!!.getString(SharedprferencesDetails.username))
        jsonObject.addProperty("post_comment", post_comment)
        jsonObject.addProperty("user_id", sharedprferencesDetails!!.getString(SharedprferencesDetails.user_id))
        jsonObject.addProperty("post_id", param1!!.post_id)
        submitComment(jsonObject)

    }

    fun submitComment(login: JsonObject) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.submit_comment(login)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "login-${t!!}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity!!, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
                Log.e("response_onResponse", "login-${response!!.body().toString()}" + response.code())
                progressDialog.dismiss()
                if (response.code() == 200 || response.code() == 201 || response.code() == 202) {
                    if (!response.body().toString().equals("null")) {
                        val intStatus: Int = response.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                            pagePost = 1
                            loading = true
                            linearListComment.removeAllViews()
                            onResume()
                        } else {
                            if (activity != null)
                                if (response.body()!!.has("message"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (response.body()!!.has("data"))
                                    Toast.makeText(activity!!, "" + response.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                        }

                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
