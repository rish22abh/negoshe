package com.iws.negoshe.user.fragment


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.google.gson.JsonObject
import com.iws.commontask.model.request.PostList
import com.iws.commontask.utils.ApiClient
import com.iws.commontask.utils.SharedprferencesDetails
import com.iws.negoshe.R
import com.iws.negoshe.user.inerfacePkg.FragmentCommunication
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_post_user.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.util.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PostUserFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PostUserFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var pagePost: Int = 1
    private var postAdapter: PostAdapter? = null
    private var listPostList: ArrayList<PostList>? = null
    private val TYPE_HEADER = 0
    private val TYPE_OTHER = 1
    private var fragmentCommunication: FragmentCommunication? = null
    private val CAMERA_PERMISSION_CODE = 23
    private val GALLERY_PERMISSION_CODE = 24
    private var sharedprferencesDetails: SharedprferencesDetails? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentCommunication = context as FragmentCommunication
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_user, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PostUserFragment.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                PostUserFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }

    private var loading = true
    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listPostList = ArrayList()

        sharedprferencesDetails = SharedprferencesDetails.getInstance(activity!!)

        val layoutManager = LinearLayoutManager(activity!!)
        recyclerViewPosts.layoutManager = layoutManager
        recyclerViewPosts.addItemDecoration(DividerItemDecoration(recyclerViewPosts.context,
                layoutManager.orientation))
        postAdapter = PostAdapter()
        recyclerViewPosts.adapter = postAdapter

        // Pagination
        recyclerViewPosts.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, newState: Int, dy: Int) {
                super.onScrolled(recyclerView, newState, dy)
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false
                            pagePost += 1
                            onResume()
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        })
    }

    inner class PostAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun getItemViewType(position: Int): Int {
            return if (position == TYPE_HEADER)
                TYPE_HEADER
            else
                TYPE_OTHER
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return if (viewType == TYPE_HEADER) {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post_header, parent, false)
                MyViewHolderHeader(view)
            } else {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post_footer, parent, false)
                MyViewHolderFooter(view)
            }
        }

        override fun getItemCount(): Int {
            return listPostList!!.size + 1
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is MyViewHolderHeader) {
                holder.bind(position - 1)
                if (finalFile != null)
                    holder.edtSearch.setText(finalFile!!.name)
                else
                    holder.edtSearch.setText("")

                holder.edtTitle.setText("")
                holder.edtPost.setText("")

                if (sharedprferencesDetails!!.getString(SharedprferencesDetails.profile_owner).equals("1"))
                    holder.buttonPost.background = ContextCompat.getDrawable(activity!!, R.drawable.button_gradient_user)
                else
                    holder.buttonPost.background = ContextCompat.getDrawable(activity!!, R.drawable.button_gradient_mechanic)
            } else {
                Picasso.with(activity!!).load(listPostList!![position - 1].post_image)
                        .placeholder(R.drawable.ic_placeholder_image).error(R.drawable.ic_placeholder_image).into((holder as MyViewHolderFooter).imagePost)
                holder.textTitle.text = listPostList!![position - 1].post_title
                holder.textDescription.text = listPostList!![position - 1].post_description
                holder.bind(position - 1)
            }
        }

        inner class MyViewHolderHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var edtSearch = itemView.findViewById<EditText>(R.id.edtSearch)
            var textPost = itemView.findViewById<TextView>(R.id.textPost)
            var viewPost = itemView.findViewById<View>(R.id.viewPost)
            var textMyPosts = itemView.findViewById<TextView>(R.id.textMyPosts)
            var viewMyPosts = itemView.findViewById<View>(R.id.viewMyPosts)
            var button_search = itemView.findViewById<ImageView>(R.id.button_search)

            var edtTitle = itemView.findViewById<EditText>(R.id.edtTitle)
            var edtPost = itemView.findViewById<EditText>(R.id.edtPost)
            var buttonPost = itemView.findViewById<Button>(R.id.buttonPost)
            var constraintPost = itemView.findViewById<ConstraintLayout>(R.id.constraintPost)
            var constraintMyPosts = itemView.findViewById<ConstraintLayout>(R.id.constraintMyPosts)
            var frame = itemView.findViewById<FrameLayout>(R.id.frame)
            fun bind(position: Int) {
                frame.setOnClickListener {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            requestPermissions(arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                    CAMERA_PERMISSION_CODE)
                        } else {
                            openDialog()
                        }
                    } else {
                        openDialog()
                    }
                }
                buttonPost.setOnClickListener {
                    if (finalFile != null) {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), finalFile!!)
                        val post_image: MultipartBody.Part = MultipartBody.Part.createFormData("post_image", finalFile!!.name, reqFile)

                        val post_title: RequestBody = RequestBody.create(MediaType.parse("text/plain"), edtTitle.text.toString())
                        val post_description: RequestBody = RequestBody.create(MediaType.parse("text/plain"), edtPost.text.toString())
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.username).replace("\"", ""))

                        createPost(
                                post_title,
                                post_description,
                                username,
                                post_image)
                    } else {
                        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), "")
                        val post_image: MultipartBody.Part = MultipartBody.Part.createFormData("post_image", "", reqFile)

                        val post_title: RequestBody = RequestBody.create(MediaType.parse("text/plain"), edtTitle.text.toString())
                        val post_description: RequestBody = RequestBody.create(MediaType.parse("text/plain"), edtPost.text.toString())
                        val username: RequestBody = RequestBody.create(MediaType.parse("text/plain"), sharedprferencesDetails!!.getString(SharedprferencesDetails.username).replace("\"", ""))

                        createPost(
                                post_title,
                                post_description,
                                username,
                                post_image)
                    }
                }

                constraintPost.setOnClickListener {
                    textPost.setTextColor(ContextCompat.getColor(activity!!, R.color.colorEdtHintUser))
                    viewPost.visibility = View.VISIBLE
                    textMyPosts.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
                    viewMyPosts.visibility = View.GONE
                    getPost(false)
                }

                constraintMyPosts.setOnClickListener {
                    textPost.setTextColor(ContextCompat.getColor(activity!!, R.color.colorAccentUser))
                    viewPost.visibility = View.GONE
                    textMyPosts.setTextColor(ContextCompat.getColor(activity!!, R.color.colorEdtHintUser))
                    viewMyPosts.visibility = View.VISIBLE
                    getPost(true)

                }
                edtSearch.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                edtTitle.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                edtPost.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Regular.otf")
                buttonPost.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Bold.otf")
                textPost.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textMyPosts.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
            }
        }

        inner class MyViewHolderFooter(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imagePost = itemView.findViewById<ImageView>(R.id.imagePost)
            var textTitle = itemView.findViewById<TextView>(R.id.textTitle)
            var textDescription = itemView.findViewById<TextView>(R.id.textDescription)
            fun bind(position: Int) {
                itemView.setOnClickListener {
                    fragmentCommunication!!.changeFragment(PostDetailFragment.newInstance(listPostList!![position], ""), listPostList!![position].post_title!!)
                }

                textTitle.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Medium.otf")
                textDescription.typeface = Typeface.createFromAsset(activity!!.assets, "fonts/Montserrat-Light.otf")
            }
        }
    }


    fun createPost(post_title: RequestBody, post_description: RequestBody, username: RequestBody, post_image: MultipartBody.Part) {

        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.submit_post(username, post_title, post_description, post_image)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "createJob-${t!!.message}")
                progressDialog.dismiss()
                when (t) {
                    is ApiClient.NoConnectivityException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    is SocketTimeoutException -> Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
                Log.e("response_onResponse", "createJob-${t!!.body().toString()}")
                progressDialog.dismiss()
                if (t.code() == 200 || t.code() == 201 || t.code() == 202) {
                    if (!t.body()!!.toString().equals(null)) {
                        val intStatus: Int = t.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            if (activity != null) {
                                Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                finalFile = null
//                                postAdapter!!.notifyDataSetChanged()
                                pagePost = 1
                                loading = true
                                listPostList!!.clear()
                                postAdapter!!.notifyDataSetChanged()
                                onResume()
                            }
                        } else {
                            if (activity != null) {
                                if (t.body()!!.has("message"))
                                    Toast.makeText(activity, "" + t.body()!!.get("message").asString, Toast.LENGTH_SHORT).show()
                                else if (t.body()!!.has("data"))
                                    Toast.makeText(activity, "" + t.body()!!.get("data").asString, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        getPost(false)
    }


    private fun getPost(post: Boolean) {
        val progressDialog: ProgressDialog = ProgressDialog(activity!!)
        progressDialog.setMessage("Loading!!")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val apiService = ApiClient.getClient(activity!!).create(ApiClient.ApiInterface::class.java)
        val call = apiService.post_list(pagePost)
        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
                Log.e("response_onFailure", "getJobType-${t!!.message}")
                progressDialog.dismiss()
                if (!call!!.isCanceled) {
                    when (t) {
                        is ApiClient.NoConnectivityException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                        is SocketTimeoutException -> if (activity != null) Toast.makeText(activity, "" + t.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onResponse(call: Call<JsonObject>?, t: Response<JsonObject>?) {
//                Log.e("response_onResponse", "getJobType-${t!!.body()!!.toString()}")
                progressDialog.dismiss()
                if (t!!.code() == 200 || t.code() == 201 || t.code() == 202) {
                    if (!t.body()!!.equals(null)) {
//                        listPostList!!.clear()
                        val intStatus: Int = t.body()!!.get("status").asInt
                        if (intStatus == 1) {
                            val jsonArray = t.body()!!.getAsJsonArray("message")
                            if (jsonArray.size() != 5) {
                                loading = false
                            }
                            for (i in 0 until jsonArray.size()) {
                                if (!post) {
                                    val jsonObject = jsonArray[i].asJsonObject
                                    val postList = PostList()
                                    postList.post_id = jsonObject.get("post_id").asInt
                                    postList.user_id = jsonObject.get("user_id").asInt
                                    postList.post_title = jsonObject.get("post_title").asString
                                    postList.post_description = jsonObject.get("post_description").asString
                                    postList.post_image = ApiClient.BASE_URL_ImagePost + jsonObject.get("post_image").asString
                                    listPostList!!.add(postList)
                                } else {
                                    val jsonObject = jsonArray[i].asJsonObject
                                    if (sharedprferencesDetails!!.getString(SharedprferencesDetails.user_id).toInt() == jsonObject.get("user_id").asInt) {
                                        val postList = PostList()
                                        postList.post_id = jsonObject.get("post_id").asInt
                                        postList.user_id = jsonObject.get("user_id").asInt
                                        postList.post_title = jsonObject.get("post_title").asString
                                        postList.post_description = jsonObject.get("post_description").asString
                                        postList.post_image = ApiClient.BASE_URL_ImagePost + jsonObject.get("post_image").asString
                                        listPostList!!.add(postList)
                                    }
                                }
                            }
                            postAdapter!!.notifyItemChanged(0)
                            postAdapter!!.notifyDataSetChanged()
                        }
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.error_500), Toast.LENGTH_SHORT).show()
                }

            }
        })
    }

    private val HARDWARECAMERA_PERMISSION_CODE = 0

    private fun openDialog() {
        val dialog1 = Dialog(activity!!)
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.setContentView(R.layout.camera_dialog_box)
        //        Button dialogButtonOK = (Button) dialog1.findViewById(R.id.ok_btn);
        val take_photo = dialog1.findViewById(R.id.textTakePhoto) as TextView
        val tv = dialog1.findViewById(R.id.textHeading) as TextView
        val gallery = dialog1.findViewById(R.id.textGallery) as TextView
        val cancel = dialog1.findViewById(R.id.textCancel) as TextView
        dialog1.show()


        take_photo.setOnClickListener {
            dialog1.dismiss()
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, HARDWARECAMERA_PERMISSION_CODE)
        }

        gallery.setOnClickListener {
            dialog1.dismiss()
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT//
            startActivityForResult(Intent.createChooser(intent, "Select File"), GALLERY_PERMISSION_CODE)
        }

        cancel.setOnClickListener { dialog1.dismiss() }
    }

    private var finalFile: File? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == HARDWARECAMERA_PERMISSION_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val image = data.extras!!.get("data") as Bitmap
//                profile_image.setImageBitmap(image);

                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                val tempUri = getImageUri(activity!!, image)

                Log.e("File", "file-" + getRealPathFromURI(tempUri))
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                finalFile = File(getRealPathFromURI(tempUri))

            }
        } else if (requestCode == GALLERY_PERMISSION_CODE && resultCode == Activity.RESULT_OK) {
            var bm: Bitmap? = null
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data.data)
//                    profile_image.setImageBitmap(bm);

                    val uri = data.data
                    Log.e("File", "file-" + getRealPathFromURI(uri))
                    finalFile = File(getRealPathFromURI(uri))
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {

        }
    }

    private fun getRealPathFromURI(contentURI: Uri): String {
        val cursor = activity!!.contentResolver.query(contentURI, null, null, null, null)
        cursor.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }
}
