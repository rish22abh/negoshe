package com.iws.negoshe.user.inerfacePkg

import android.support.v4.app.Fragment

interface FragmentCommunication {
    fun changeFragment(fragment: Fragment, title: String)
}