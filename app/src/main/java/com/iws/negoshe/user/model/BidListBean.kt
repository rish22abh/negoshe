package com.iws.negoshe.user.model

class BidListBean {
    var username: String? = null
    var distance: Double? = null
    var first_name: String? = null
    var last_name: String? = null
    var working_hours: Int? = null
    var profile_pic: String? = null
    var bid_image: String? = null
    var bid_id: Int? = null
    var quote_amount: Int? = null
    var mechanic_lat: Double? = null
    var mechanic_lng: Double? = null
    var mechnic_rating__avg: Float? = null
}