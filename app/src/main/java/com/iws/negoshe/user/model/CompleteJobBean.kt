package com.iws.negoshe.user.model

class CompleteJobBean {
    var profile_pic: String? = null
    var job_image: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var job_id: Int? = null
    var job_rating: Int? = null
    var job_amount: Int? = null
    var job_title: String? = null
    var job_feedback: String? = null
    var job_nature: String? = null
    var mechanic_profile: String? = null
    var mechanic_username: String? = null
}