package com.iws.negoshe.user.model

class PendingJobBean {
    var profile_pic: String? = null
    var is_active: String? = null
    var job_id: Int? = null
    var job_title: String? = null
    var job_image: String? = null
}