package com.iws.commontask.model.request

class CommentListBean {
    var post_id: Int? = null
    var post_comment: String? = null
    var username: String? = null
    var comment_date_added: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var profile_pic: String? = null
}