package com.iws.commontask.model.request

import android.os.Parcel
import android.os.Parcelable

class PostList() :Parcelable{
    var post_id: Int? = null
    var user_id: Int? = null
    var post_title: String? = null
    var post_description: String? = null
    var post_image: String? = null

    constructor(parcel: Parcel) : this() {
        post_id = parcel.readValue(Int::class.java.classLoader) as? Int
        user_id = parcel.readValue(Int::class.java.classLoader) as? Int
        post_title = parcel.readString()
        post_description = parcel.readString()
        post_image = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(post_id)
        parcel.writeValue(user_id)
        parcel.writeString(post_title)
        parcel.writeString(post_description)
        parcel.writeString(post_image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PostList> {
        override fun createFromParcel(parcel: Parcel): PostList {
            return PostList(parcel)
        }

        override fun newArray(size: Int): Array<PostList?> {
            return arrayOfNulls(size)
        }
    }
}