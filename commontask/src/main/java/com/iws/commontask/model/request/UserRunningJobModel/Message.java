
package com.iws.commontask.model.request.UserRunningJobModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message implements Parcelable{

    @SerializedName("user_status")
    @Expose
    private Boolean userStatus;
    @SerializedName("job_image")
    @Expose
    private String jobImage;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("payment_status")
    @Expose
    private Boolean paymentStatus;
    @SerializedName("bid_amount")
    @Expose
    private Integer bidAmount;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("mechanic_profile_pic")
    @Expose
    private String mechanicProfilePic;
    @SerializedName("shop_status")
    @Expose
    private Boolean shopStatus;
    @SerializedName("mechanic_username")
    @Expose
    private String mechanicUsername;
    @SerializedName("work_hours")
    @Expose
    private Integer workHours;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;

    protected Message(Parcel in) {
        byte tmpUserStatus = in.readByte();
        userStatus = tmpUserStatus == 0 ? null : tmpUserStatus == 1;
        jobImage = in.readString();
        lastName = in.readString();
        if (in.readByte() == 0) {
            jobId = null;
        } else {
            jobId = in.readInt();
        }
        mobile = in.readString();
        byte tmpPaymentStatus = in.readByte();
        paymentStatus = tmpPaymentStatus == 0 ? null : tmpPaymentStatus == 1;
        if (in.readByte() == 0) {
            bidAmount = null;
        } else {
            bidAmount = in.readInt();
        }
        firstName = in.readString();
        mechanicProfilePic = in.readString();
        byte tmpShopStatus = in.readByte();
        shopStatus = tmpShopStatus == 0 ? null : tmpShopStatus == 1;
        mechanicUsername = in.readString();
        if (in.readByte() == 0) {
            workHours = null;
        } else {
            workHours = in.readInt();
        }
        jobTitle = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public Boolean getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
    }

    public String getJobImage() {
        return jobImage;
    }

    public void setJobImage(String jobImage) {
        this.jobImage = jobImage;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(Integer bidAmount) {
        this.bidAmount = bidAmount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMechanicProfilePic() {
        return mechanicProfilePic;
    }

    public void setMechanicProfilePic(String mechanicProfilePic) {
        this.mechanicProfilePic = mechanicProfilePic;
    }

    public Boolean getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(Boolean shopStatus) {
        this.shopStatus = shopStatus;
    }

    public String getMechanicUsername() {
        return mechanicUsername;
    }

    public void setMechanicUsername(String mechanicUsername) {
        this.mechanicUsername = mechanicUsername;
    }

    public Integer getWorkHours() {
        return workHours;
    }

    public void setWorkHours(Integer workHours) {
        this.workHours = workHours;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (userStatus == null ? 0 : userStatus ? 1 : 2));
        dest.writeString(jobImage);
        dest.writeString(lastName);
        if (jobId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(jobId);
        }
        dest.writeString(mobile);
        dest.writeByte((byte) (paymentStatus == null ? 0 : paymentStatus ? 1 : 2));
        if (bidAmount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(bidAmount);
        }
        dest.writeString(firstName);
        dest.writeString(mechanicProfilePic);
        dest.writeByte((byte) (shopStatus == null ? 0 : shopStatus ? 1 : 2));
        dest.writeString(mechanicUsername);
        if (workHours == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(workHours);
        }
        dest.writeString(jobTitle);
    }
}
