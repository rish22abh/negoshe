package com.iws.commontask.utils

import android.content.Context
import android.net.ConnectivityManager
import com.google.gson.JsonObject
import com.iws.commontask.model.request.GetJobSkillBean
import com.iws.commontask.model.request.UserRunningJobModel.UserOngoingJobBean
import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import retrofit2.http.Headers
import java.io.IOException
import java.util.concurrent.TimeUnit

class ApiClient {
    companion object {
        private val BASE_URL = "http://165.227.88.236:9090/api/v1/"
        val BASE_URL_Image = "http://165.227.88.236:9090/media/"
        val BASE_URL_ImagePost = "http://165.227.88.236:9090/"
        private var retrofit: Retrofit? = null
        private var context: Context? = null
        fun getClient(context: Context): Retrofit {
            this.context = context
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(provideOkHttpClient())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return retrofit!!
        }

        fun isOnline(): Boolean {
            val connectivityManager = context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }

        private fun provideOkHttpClient(): OkHttpClient {
            val okhttpClientBuilder = OkHttpClient.Builder()
            okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
            okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
            okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)
            okhttpClientBuilder.addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain?): Response {
                    if (!isOnline()) {
                        throw NoConnectivityException()
                    }

                    val builder = chain!!.request().newBuilder()
                    return chain.proceed(builder.build())
                }

            })
            return okhttpClientBuilder.build()
        }
    }


    interface ApiInterface {
        @Headers(
                "Accept: application/json",
                "Content-Type: application/json; charset=ascii"
        )
        @POST("user_login/")
        fun user_login(@Body login: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json; charset=ascii"
        )
        @POST("place_bid/")
        fun place_bid(@Body place_bid: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @PATCH("change_nego_pwd/")
        fun change_nego_pwd(@Body change: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("user_sign_out/")
        fun user_sign_out(@Body user_sign_out: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/x-www-form-urlencoded"
        )
        @POST("user_register/")
        @FormUrlEncoded
        fun user_register(@Field("password") password: String,
                          @Field("email") email: String,
                          @Field("first_name") first_name: String,
                          @Field("last_name") last_name: String,
                          @Field("mobile") mobile: String,
                          @Field("user_flag") user_flag: String): Call<JsonObject>


        @Multipart
        @POST("create_job/")
        fun create_job(@Part("job_title") job_title: RequestBody,
                       @Part("work_nature") work_nature: RequestBody,
                       @Part("added_by") added_by: RequestBody,
                       @Part("address") address: RequestBody,
                       @Part quote_img: MultipartBody.Part): Call<JsonObject>


        @Multipart
        @POST("submit_post/")
        fun submit_post(@Part("username") username: RequestBody,
                        @Part("post_title") post_title: RequestBody,
                        @Part("post_description") post_description: RequestBody,
                        @Part post_image: MultipartBody.Part): Call<JsonObject>

        @Multipart
        @PATCH("update_profile/")
        fun update_profile(@Part("username") username: RequestBody,
                           @Part("mobile") mobile: RequestBody,
                           @Part("address") address: RequestBody,
                           @Part("user_first_name") user_first_name: RequestBody,
                           @Part("user_last_name") user_last_name: RequestBody,
                           @Part("website") website: RequestBody,
                           @Part("zipcode") zipcode: RequestBody,
                           @Part profile_pic: MultipartBody.Part): Call<JsonObject>


        @Headers(
                "Accept: application/json",
                "Content-Type: application/json; charset=ascii"
        )
        @POST("forgot_password/")
        fun forgot_password(@Body forgot: JsonObject): Call<JsonObject>

        @GET("get_skill_list/")
        fun get_skill_list(): Call<GetJobSkillBean>

        @GET("post_list/")
        fun post_list(@Query("page") page: Int): Call<JsonObject>

        @GET("comment_list/")
        fun comment_list(@Query("post_id") post_id: String,
                         @Query("page") page: Int): Call<JsonObject>


        @GET("user_amount/")
        fun user_amount(@Query("username") username: String): Call<JsonObject>

        @GET("get_user_detail/")
        fun get_user_detail(@Query("user_name") username: String): Call<JsonObject>

        @GET("user_ongoing_job/")
        fun user_ongoing_job(@Query("username") username: String): Call<UserOngoingJobBean>

        @GET("complete_job_list/")
        fun complete_job_list(@Query("username") username: String,
                              @Query("user_flag") user_flag: String): Call<JsonObject>


        @GET("bid_list/")
        fun bid_list(@Query("user_id") user_id: String): Call<JsonObject>

        @Multipart
        @PATCH("update_profile/")
        fun update_profileMobile(@Part("username") userName: RequestBody,
                                 @Part("mobile") mobile: RequestBody): Call<JsonObject>

        @Multipart
        @PATCH("update_profile/")
        fun update_profileWebsite(@Part("username") userName: RequestBody, @Part("website") mobile: RequestBody): Call<JsonObject>


        @Multipart
        @PATCH("update_profile/")
        fun update_profileAddress(@Part("username") userName: RequestBody, @Part("address") mobile: RequestBody): Call<JsonObject>


        @Multipart
        @PATCH("update_profile/")
        fun update_profileZip(@Part("username") userName: RequestBody, @Part("zipcode") mobile: RequestBody): Call<JsonObject>


        @Multipart
        @PATCH("update_profile/")
        fun update_profilejob_nature(@Part("username") userName: RequestBody, @Part("job_nature") mobile: RequestBody): Call<JsonObject>


        @Multipart
        @PATCH("update_profile/")
        fun update_profileName(@Part("username") userName: RequestBody, @Part("user_first_name") user_first_name: RequestBody, @Part("user_last_name") user_last_name: RequestBody): Call<JsonObject>

        @Multipart
        @PATCH("update_profile/")
        fun update_profilePic(@Part("username") userName: RequestBody, @Part profile_pic: MultipartBody.Part): Call<JsonObject>

        @Multipart
        @PATCH("update_profile/")
        fun update_banner1(@Part("username") userName: RequestBody, @Part profile_pic: MultipartBody.Part): Call<JsonObject>

        @Multipart
        @POST("create_bid/")
        fun create_bid(@Part("job_id") job_id: RequestBody,
                       @Part("username") username: RequestBody,
                       @Part("quote_amount") quote_amount: RequestBody,
                       @Part("work_hours") work_hours: RequestBody,
                       @Part quotation_image: MultipartBody.Part): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("mechanic_job_search/")
        fun mechanic_job_search(@Body mechanic_job_search: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("mechanic_process_search/")
        fun mechanic_process_search(@Body mechanic_process_search: JsonObject): Call<JsonObject>


        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("submit_comment/")
        fun submit_comment(@Body submit_comment: JsonObject): Call<JsonObject>


        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("search_job_user/")
        fun search_job_user(@Body search_job_user: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("short_bid/")
        fun short_bid(@Body short_bid: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("complete_job/")
        fun complete_job(@Body complete_job: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @PATCH("job_feedback/")
        fun job_feedback(@Body complete_job: JsonObject): Call<JsonObject>

        @Headers(
                "Accept: application/json",
                "Content-Type: application/json"
        )
        @POST("bid_payment/")
        fun bid_payment(@Body bid_payment: JsonObject): Call<JsonObject>

    }

    class NoConnectivityException : IOException() {
        override val message: String
            get() = "Internet not working properly!!"
    }
}