package com.iws.commontask.utils

import android.content.Context
import android.content.SharedPreferences

class SharedprferencesDetails private constructor() {
    companion object {
        private val prefName: String = "negoshePref"
        val website: String = "website"
        val username: String = "username"
        val last_name: String = "last_name"
        val profile_pic: String = "profile_pic"
        val is_active: String = "is_active"
        val profile_owner: String = "profile_owner"
        val address: String = "address"
        val first_name: String = "first_name"
        val job_nature: String = "job_nature"
        val mobile: String = "mobile"
        val wallet_amount: String = "wallet_amount"
        val zipcode: String = "zipcode"
        val email: String = "email"
        val password: String = "password"
        val user_id: String = "user_id"
        val registrationId: String = "registrationId"
        val banner_img1: String = "banner_img1"
        val banner_img3: String = "banner_img3"
        val banner_img2: String = "banner_img2"
        val banner_img4: String = "banner_img4"
        private var sharedprferencesDetails: SharedprferencesDetails? = null
        private var sharedPreferences: SharedPreferences? = null

        fun getInstance(context: Context): SharedprferencesDetails {
            if (sharedprferencesDetails == null) {
                sharedprferencesDetails = SharedprferencesDetails()
                sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
            }
            return sharedprferencesDetails!!
        }


    }

    fun putString(key: String, value: String) {
        sharedPreferences!!.edit().putString(key, value).apply()
    }

    fun getString(key: String): String {
        return sharedPreferences!!.getString(key, "")
    }

    fun clear() {
        val registrationToken = sharedPreferences!!.getString(SharedprferencesDetails.registrationId, "")
        sharedPreferences!!.edit().clear().apply()
        putString(SharedprferencesDetails.registrationId, registrationToken)
    }
}